<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Loader extends CI_Loader {
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	 * Load View
	 *
	 * This function is used to load a "view" file.  It has three parameters:
	 *
	 * 1. The name of the "view" file to be included.
	 * 2. An associative array of data to be extracted for use in the view.
	 * 3. TRUE/FALSE - whether to return the data or load it.  In
	 * some cases it's advantageous to be able to return data so that
	 * a developer can process it in some way.
	 *
	 * @param	string
	 * @param	array
	 * @param	bool
	 * @return	void
	 */
	public function view($view, $vars = array(), $return = FALSE)
	{
		$vars = (array) $vars;
		if(isset($vars['template'])){
			$vars['view'] = $view;
			$view = $vars['template'];
		}
		
		$vars = $this->html_escape($vars);
		
		return $this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_object_to_array($vars), '_ci_return' => $return));
	}
	
	#MODIFIED COPY OF CI's COMMON FUNCTION
	private function html_escape($var)
	{	
		if(($is_object = is_object($var)) || is_array($var)){
			$escaped = array_map(array($this, 'html_escape'), (array) $var);
			return $is_object === TRUE ? (object) $escaped : $escaped;
		}elseif(is_string($var)){
			return htmlspecialchars($var, ENT_QUOTES, config_item('charset'));
		}else{
			return $var;
		}
	}
}