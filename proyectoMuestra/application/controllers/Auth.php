<? defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {
	
	function __construct(){
		parent:: __construct();
		$this->data = new stdClass;
	}
	
	public function logout(){
		$this->load->library('ion_auth');
		$this->ion_auth->logout();
		redirect();
	}
	
	public function login(){
		if($this->session->user_id) redirect();
		if($this->input->post()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email','Correo electrónico','required|trim');
			$this->form_validation->set_rules('password','Contraseña','required|trim');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				$this->load->library('ion_auth');
				$identity = $this->input->post('email');
				$password = $this->input->post('password');
				$remember = (bool)$this->input->post('remember');
				if($this->ion_auth->login($identity,$password,$remember)){
					$this->load->model('user_model');
					$user = $this->user_model->get_user($this->session->user_id);
					$this->session->set_userdata('name',$user->name);
					$this->session->set_userdata('group',$this->ion_auth->get_users_groups($this->session->user_id)->row()->id);
					if($this->input->is_ajax_request()) scape_modal();
					else redirect('');
				}
				else $this->data->alert_danger = 'Datos incorrectos';
			}else{
				if(form_error('email')) $this->data->email_error = form_error('email');
				if(form_error('password')) $this->data->pass_error = form_error('password');
			}
		}
		$this->data->title = 'Iniciar sesión';
		if($this->input->is_ajax_request()){
			$this->load->view('auth/modal_login',$this->data);
		}else{
			$this->data->template = 'templates/master';
			$this->load->view('auth/login',$this->data);
		}
	}
	
	public function register(){
		if($this->session->user_id) redirect();
		if($this->input->post()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name','Nombre','required|trim');
			$this->form_validation->set_rules('email','Correo electrónico','required|valid_email|is_unique[tb_users.email]|trim');
			$this->form_validation->set_rules('password','Contraseña','required|alpha_numeric|min_length[5]|max_length[20]|trim');
			$this->form_validation->set_rules('passconf','Confirmar contraseña','required|matches[password]|trim');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_message('alpha_numeric','No puede tener caracteres especiales');
			$this->form_validation->set_message('valid_email','Correo invalido');
			$this->form_validation->set_message('is_unique','Correo ya registrado');
			$this->form_validation->set_message('min_length','Como minimo debe tener 5 caracteres');
			$this->form_validation->set_message('max_length','Como maximo puede tener 20 caracteres');
			$this->form_validation->set_message('matches','Contraseñas no coinciden');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				$this->load->library('ion_auth');
				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				if(!$this->ion_auth->email_check($email)){
					if($this->ion_auth->register($email,$password,$email,array('name'=>$name),array(2))){
						$user = $this->ion_auth->where('email',$email)->row();
						$this->session->set_userdata('name',$user->name);
						$this->session->set_userdata('group',$this->ion_auth->get_users_groups($this->session->user_id)->row()->id);
						$this->email_data = new stdClass;
						$this->email_data->user = $user;
						$this->email_data->subject = 'Bienvenido a boteadora';
						$this->email_data->template = 'templates/email';
						$this->email_data->url = base_url('auth/activate_email/'.$email);
						$this->email_data->message = $this->load->view('email/activate',$this->email_data,true);
						$this->load->library('email');
						$config['mailtype'] = 'html';
						$config['priority'] = '2';
						$this->email->initialize($config);
						$was_sent = $this->email
						->to($user->email)
						->from(SYSTEM_EMAIL,SYSTEM_NAME)
						->subject($this->email_data->subject)
						->message($this->email_data->message)
						->send();
						if(!$was_sent) $this->data->flash_alert_warning = 'El sistema no pudo enviar el correo';
						$this->ion_auth->login($email,$password,true);
						if($this->input->is_ajax_request()) scape_modal();
						else redirect('');
					}else $this->data->alert_warning = 'Ocurrio un problema, por favor vuelve a intentarlo';
				}else $this->data->email_error = 'Correo ya registrado';
			}else{
				if(form_error('name')) $this->data->name_error = form_error('name');
				if(form_error('email')) $this->data->email_error = form_error('email');
				if(form_error('password')) $this->data->pass_error = form_error('password');
				if(form_error('passconf')) $this->data->pass_conf_error = form_error('passconf');
			}
		}
		$this->data->title = 'Registro';
		if($this->input->is_ajax_request()){
			$this->load->view('auth/modal_register',$this->data);
		}else{
			$this->data->template = 'templates/master';
			$this->load->view('auth/register',$this->data);
		}
	}
	
	public function activate_email($email=''){
		if(!$email) redirect();
		$this->load->model('user_model');
		$this->user_model->activate($email);
		redirect_success_flash('profile/'.$user->id,'Correo activado');
	}
	
	public function forgotten_password() {
		if($this->session->user_id) redirect();
		if($this->input->post()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email','Correo electrónico','required|valid_email|trim');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_message('valid_email','Correo invalido');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				$this->load->library('ion_auth');
				$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));
				if($forgotten){
					$this->email_data = new stdClass;
					$this->email_data->user = $this->ion_auth->where('email',$forgotten['identity'])->row();
					$this->email_data->subject = 'Restaurar contraseña de tu usuario';
					$this->email_data->template = 'templates/email';
					$this->email_data->url = base_url('restablecer-contrasena/'.$forgotten['forgotten_password_code']);
					$this->email_data->message = $this->load->view('email/forgotten_password',$this->email_data,TRUE);
					$this->load->library('email');
					$was_sent = $this->email
					->to($forgotten['identity'])
					->from(SYSTEM_EMAIL,SYSTEM_NAME)
					->subject($this->email_data->subject)
					->message($this->email_data->message)
					->send();
					if(!$was_sent) $this->data->alert_warning = 'El sistema no pudo enviar el correo';
					redirect_success_flash('auth/login','Se envió el correo con exito');
				}
			}else if(form_error('email')) $this->data->email_error = form_error('email');
		}
		$this->data->title = 'Olvidé mi contraseña';
		if(!$this->input->is_ajax_request()){
			$this->data->template = 'templates/master';
			$this->load->view('auth/forgotten_password',$this->data);
		}else $this->load->view('auth/modal_forgotten_password', $this->data);
		
	}
	
	public function reset_password($code=0){
		$this->load->library('ion_auth');
		$user = $this->ion_auth->forgotten_password_check($code);
		if($user){
			if($this->input->post()){
				$this->load->library('form_validation');
				$this->form_validation->set_rules('pass','Nueva contraseña','required|min_length[5]|max_length[20]|trim');
				$this->form_validation->set_rules('conf','Confirmar contraseña','required|matches[pass]|trim');
				$this->form_validation->set_message('required','El campo es obligatorio');
				$this->form_validation->set_message('matches','Contraseñas no coinciden');
				$this->form_validation->set_message('min_length','Minimo 5 cararcteres');
				$this->form_validation->set_message('max_length','Como maximo puede tener 20 caracteres');
				$this->form_validation->set_error_delimiters('','');
				if($this->form_validation->run() === true){
					if($this->ion_auth->reset_password($user->email, $this->input->post('pass')))
						redirect_success_flash('auth/login','Contraseña establecida con exito');
					else redirect_warning_flash('auth/login','Ocurrio un error durante la actualización');
				}else{
					if(form_error('pass')) $this->data->pass_error = form_error('pass');
					if(form_error('conf')) $this->data->conf_error = form_error('conf');
				}
			}
		} else redirect();
		$this->data->title = 'Restablece tu contraseña';
		$this->data->template = 'templates/master';
		$this->load->view('auth/reset_password',$this->data);
	}
}
?>