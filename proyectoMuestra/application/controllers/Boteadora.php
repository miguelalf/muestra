<? defined('BASEPATH') OR exit('No direct script access allowed');

class Boteadora extends CI_Controller {
	
	function __construct(){
		parent:: __construct();
		$this->data = new stdClass;
	}
	
	public function index(){
		$this->load->model('project_model', 'projects');
		$this->load->library('timeflow');
		$this->data->top_project = $this->projects->get_top_project();
		$this->data->projects = $this->projects->get_projects(array('main' => true));
		if(empty($this->data->pojects)){
			$this->load->library('dummy');
			$this->data->dummy_project = $this->dummy;
		}
		$this->data->template = 'templates/master';
		$this->load->view('pages/home',$this->data);
	}
	
	public function how_support(){
		if(!$this->input->is_ajax_request()) show_404();
		$this->data->title = '¿Cómo apoyar?';
		$this->load->view('pages/modal_support', $this->data);
	}
	
	public function how_works(){
		if(!$this->input->is_ajax_request()) show_404();
		$this->data->title = '¿Cómo funciona?';
		$this->load->view('pages/modal_works', $this->data);
	} 
	
	public function us(){
		if(!$this->input->is_ajax_request()) show_404();
		$this->data->title = 'Nosotros';
		$this->load->view('pages/modal_us', $this->data);
	}
	
	public function terms(){
		$this->data->title = 'Términos y condiciones de uso de boteadora';
		if(!$this->input->is_ajax_request()){
			$this->data->col = 8;
			$this->data->col_offset = 2;
			$this->data->bclass = 'section-gray';
			$this->data->template = 'templates/boteadora';
			$this->load->view('pages/terms',$this->data);
		}else $this->load->view('pages/modal_terms',$this->data);
	}
	
	public function connect(){
		if($this->input->post()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('u-name','','required|trim');
			$this->form_validation->set_rules('u-email','','required|trim');
			$this->form_validation->set_rules('u-message','','required|trim');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				redirect_success_flash('contacto','Correo enviado con exito');
			}else{
				if(form_error('u-name')) $this->data->name_error = form_error('u-name');
				if(form_error('u-email')) $this->data->email_error = form_error('u-email');
				if(form_error('u-message')) $this->data->message_error = form_error('u-message');
			}
		}
		$this->data->col = 8;
		$this->data->col_offset = 2;
		$this->data->title = 'Contacto';
		$this->data->bclass = 'section-gray';
		if($this->input->is_ajax_request()){
			unset($this->data->template);
			$this->load->view('pages/modal_connect', $this->data);
		}
		else{
			$this->data->template = 'templates/boteadora';
			$this->load->view('pages/connect',$this->data);
		}
	}
	
	public function not_found(){
		$this->data->template = 'templates/boteadora';
		$this->load->view('errors/page_not_found',$this->data);
	}
	
}