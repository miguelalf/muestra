<? defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {
	
	function __construct(){
		parent:: __construct();
		$this->data = new stdClass;
		$this->load->library('timeflow');
	}
	
	function index($current_page=1){
		$this->load->model('project_model');
		$per_page = 9; $total = 0;
		$constrains['limit']  = $per_page;
		$constrains['offset'] = ($current_page-1)*$per_page;
		if($this->input->post()){
			$constrains['category'] = $this->input->post('category');
			$constrains['name'] = $this->input->post('search');
		}
		$this->data->projects = $this->project_model->get_projects($constrains,$total);
		$config['suffix']		= !empty($_SERVER['QUERY_STRING']) ? '?'.$this->input->server('QUERY_STRING') : '';
		$config['base_url']		= base_url('projects');
		$config['first_url']	= base_url('projects').$config['suffix'];
		$config['total_rows']	= $total;
		$config['per_page']		= $per_page;
		$config['uri_segment']  = 2;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$this->data->col = '10';
		$this->data->col_offset = '1';
		$this->data->title = 'Todas las campañas';
		$this->data->bclass = 'section-gray';
		$this->data->template = 'templates/boteadora';
		$this->data->categories = $this->project_model->get_categories();
		$this->load->view('project/search',$this->data);
	}
	
	public function new_project(){
		$this->load->model(array('project_model'));
		if($this->input->post()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title','Nombre','required|trim');
			$this->form_validation->set_rules('description','Descripción','required|trim');
			$this->form_validation->set_rules('category','Categoría','required');
			$this->form_validation->set_rules('goal','Meta','required|trim');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				$this->file = $_FILES['image'];
				$this->project_data = new stdClass();
				if(file_exists($this->file['tmp_name']) || is_uploaded_file($this->file['tmp_name'])){
					$config['upload_path']   = PUBLIC_PATH.'project/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size']      = 5120;
					$config['file_name']     = bin2hex(openssl_random_pseudo_bytes(12));
					$this->load->library('upload', $config);
					if($this->upload->do_upload('image')){
						$_data = $this->upload->data();
						$this->crop_image($_data);
						$this->project_data->image = $_data['raw_name'].'_thumb'.$_data['file_ext'];
						$this->project_data->user_id = $this->session->user_id;
						$this->project_data->title = $this->input->post('title');
						$this->project_data->presentation = $this->input->post('description');
						$this->project_data->category = $this->input->post('category');
						$this->project_data->content = $this->input->post('content');
						$this->project_data->p_goal = $this->input->post('goal');
						$this->project_data->n_days = $this->input->post('days');
						$this->project_data->create_date = time();
						if($project_id = $this->project_model->new_project($this->project_data)){
							$this->data->alert_success = 'Proyecto creado con exito';
							redirect('project/settings/'.$project_id);
						}else $this->data->alert_warning = 'Ocurrio un error, por favor vuelve a intentar';
					}else $this->data->alert_warning = 'Ocurrio un error, por favor vuelve a intentar';
				}else $this->data->image_error = 'Campo obligatorio';
			}else{
				if(validation_errors()) $this->data->alert_warning = 'Errores de validación';
				if(form_error('title')) $this->data->title_error = form_error('title');
				if(form_error('description')) $this->data->description_error = form_error('description');
				if(form_error('category')) $this->data->category_error = form_error('category');
				if(form_error('goal')) $this->data->goal_error = form_error('goal');
			}
		}
		$this->data->col        = '10';
		$this->data->col_offset = '1';
		$this->data->categories = $this->project_model->get_categories();
		$this->data->template   = 'templates/boteadora';
		$this->data->bclass     = 'section-gray';
		$this->data->title      = 'Nuevo proyecto';
		$this->load->view('project/new',$this->data);
	}
	
	public function project_settings($project_id=null){
		$this->load->model(array('project_model'));
		if(!$project_id) redirect('boteadora/not_found');
		if(!$this->project_model->is_my_project($this->session->user_id,$project_id)) redirect('boteadora/not_found');
		$this->data->project = $this->project_model->get_project($project_id);
		if($this->input->post()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title','Nombre','required|trim');
			$this->form_validation->set_rules('description','Descripción','required|trim');
			$this->form_validation->set_rules('category','Categoría','required');
			$this->form_validation->set_rules('goal','Categoría','required');
			$this->form_validation->set_rules('days','Categoría','required');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				$this->file = $_FILES['image'];
				$data['title']        = $this->input->post('title');
				$data['presentation'] = $this->input->post('description');
				$data['content']      = $this->input->post('content');
				$data['category']     = $this->input->post('category');
				$data['p_goal']       = round($this->input->post('goal'),2);
				$data['n_days']       = $this->input->post('days');
				if(file_exists($this->file['tmp_name']) || is_uploaded_file($this->file['tmp_name'])){
					$config['upload_path'] = PUBLIC_PATH.'project/';
					$config['allowed_types'] = 'jpg|jpeg|png';
					$config['max_size'] = 5120;
					$config['file_name'] = bin2hex(openssl_random_pseudo_bytes(12));
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('image')){
						$_data = $this->upload->data();
						$this->crop_image($_data);
						$target = explode('_thumb',$this->data->project->image);
						@unlink(PUBLIC_PATH.'project/'.$target[0].$target[1]);
						@unlink(PUBLIC_PATH.'project/'.$this->data->project->image);
						$data['image'] = $_data['raw_name'].'_thumb'.$_data['file_ext'];
					}else $this->data->alert_warning = 'Ocurrio un error mientras se guardaba la imagen';
				}
				if($this->project_model->update_project($project_id,$data)) $this->data->alert_success = 'Guardado con exito';
				else $this->data->alert_warning = 'Ocurrio un error mientras se actualizaba el proyecto';
				$this->data->project = $this->project_model->get_project($project_id);
			}else{
				if(validation_errors()) $this->data->alert_warning = 'Faltan datos obligatorios';
				if(form_error('title')) $this->data->title_error = form_error('title');
				if(form_error('description')) $this->data->description_error = form_error('description');
				if(form_error('category')) $this->data->category_error = form_error('category');
				if(form_error('goal')) $this->data->goal_error = form_error('goal');
				if(form_error('days')) $this->data->days_error = form_error('days');
			}
		}
		$this->data->col        = '10';
		$this->data->col_offset = '1';
		$this->data->bclass     = 'section-gray';
		$this->data->categories = $this->project_model->get_categories();
		$this->data->title      = 'Configuración de '.$this->data->project->title;
		$this->data->template   = 'templates/boteadora';
		$this->load->view('project/settings',$this->data);
	}
	
	public function faqs($project_id=0){
		if(!$this->input->is_ajax_request()) show_404;
		$this->data->title = 'Preguntas frecuentes';
		$this->data->content = 'Ups... no hay contenido por el momento';
		$this->load->view('project/modal_faqs',$this->data);
	}
	
	public function all($user_id=null,$current_page=1){
		if(!$user_id) redirect();
		$this->load->model(array('project_model'));
		$per_page = 6; $total = 0;
		$constrains['limit']  = $per_page;
		$constrains['offset'] = ($current_page-1)*$per_page;
		$this->data->projects = $this->project_model->get_projects_by_id($user_id,$constrains,$total);
		$config['suffix']		= !empty($_SERVER['QUERY_STRING']) ? '?'.$this->input->server('QUERY_STRING') : '';
		$config['base_url']		= base_url('projects/all/'.$user_id.'/');
		$config['first_url']	= base_url('projects/all/'.$user_id.'/').$config['suffix'];
		$config['total_rows']	= $total;
		$config['per_page']		= $per_page;
		$config['uri_segment']  = 4;
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		$this->data->user_id    = $user_id;
		$this->data->title      = 'Lista de proyectos';
		$this->data->bclass     = 'section-gray';
		$this->data->col        = '10';
		$this->data->col_offset = '1';
		$this->data->template   = 'templates/boteadora';
		$this->data->in_view	= true;
		$this->load->view('project/list',$this->data);
	}
	
	public function view($project_id=null){
		if(!$project_id){
			$this->load->library('dummy');
			$this->data->project = $this->dummy;
		}else{
			$this->load->model('project_model');
			$this->data->project = $this->project_model->get_project($project_id);
		}
		$this->load->model('user_model','users');
		if($this->session->user_id) $this->data->user = $this->users->get_user($this->session->user_id);
		$this->data->active     = $this->timeflow->active($this->data->project->start_date,$this->data->project->n_days);
		$this->data->col        = 10;
		$this->data->col_offset = 1;
		$this->data->title      = $this->data->project->title;
		$this->data->bclass     = 'section-gray';
		$this->data->template   = 'templates/master';
		$this->load->view('project/view',$this->data);
	}
	
	public function report($project_id=0){
		if(!$this->input->is_ajax_request()) show_404();
		$this->load->model(array('user_model','project_model'));
		$this->load->library(array('form_validation'));
		$this->data->user = $this->user_model->get_user($this->session->user_id);
		if($this->input->post()){
			$this->form_validation->set_rules('email','','required|trim');
			$this->form_validation->set_rules('name','','required|trim');
			$this->form_validation->set_rules('comment','','required|trim');
			$this->form_validation->set_message('required','Campo obligatorio');
			$this->form_validation->set_error_delimiters('','');
			if($this->form_validation->run() === true){
				$data = array(
					'project_id' => $project_id,
					'user_id' 	 => @$this->session->user_id ? : 0,
					'email' 	 => @$this->session->user_id ? null : $this->input->post('email'),
					'comment' 	 => $this->input->post('comment')
				);
				$this->project_model->add_report($data);
				scape_modal();
			}else{
				$this->data->email_error   = @form_error('email');
				$this->data->name_error    = @form_error('name');
				$this->data->comment_error = @form_error('comment');
			}
		}
		$this->data->project_id = $project_id;
		$this->data->title = 'Forma de reporte';
		$this->load->view('project/modal_report',$this->data);
	}
	
	private function crop_image($data){
		$this->load->library('image_lib');
		$config['source_image'] = $data['full_path'];
		$config['create_thumb'] = TRUE;
		$config['width']        = 1200;
		$config['height']       = 800;
		$this->image_lib->initialize($config);
		if(!$this->image_lib->fit()){
			xd($this->image_lib->display_errors());
		}
	}
}
?>