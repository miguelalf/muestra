<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Timeflow{
	
	private $CI;
	
	function __construct(){;
		$this->data = new stdClass;
		$this->CI = &get_instance();
	}
	
	function days_left($date, $days=0){
		$interval = date_diff(date_create(date('Y-m-d',time())), date_create(date('Y-m-d',strtotime('+'.$days.' days',$date))));
		return $interval->format('Faltan %a días');
	}
	
	function active($date, $days=0){
		$ndate = strtotime('+'.$days.' days',$date);
		return $ndate > time() ? true : false;
	}
	
}