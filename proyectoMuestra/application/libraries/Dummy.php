<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dummy{
	public $id;
	public $user_id;
	public $image;
	public $accepted;
	public $title;
	public $name;
	public $user_image;
	public $presentation;
	public $content;
	public $p_goal;
	public $p_amount;
	public $n_days;
	public $start_date;
	public $backers;
	
	function __construct(){
		$this->id = 0; 
		$this->user_id = 1; 
		
		$CI = &get_instance();
		$CI->load->model('user_model','users');
		$user = $CI->users->get_user($this->user_id);
		
		$this->image = 'dummy_thumb.jpg';
		$this->accepted = 0;
		$this->title = 'Por un mundo feliz';
		$this->name = $user->name;
		$this->user_image = $user->image;
		$this->presentation = 'Todos tenemos derecho a ser felices';
		$this->content = 'Todos tenemos derecho a ser felices hasta el último momento de nuestras vidas';
		$this->p_goal = 1000;
		$this->p_amount = 0;
		$this->n_days = 90;
		$this->start_date = null;
		$this->backers = new stdClass();
		$this->backers->count = 0;
	}
}