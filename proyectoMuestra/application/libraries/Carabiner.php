<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('carabiner_base.php');

class Carabiner extends Carabiner_base {
	protected $groups;
	
	function __construct()
	{
	    parent::__construct();
		$this->groups = array();
	}
	
	#PREVENT EMPTY LINKS
	protected function _tag($flag, $ref, $cache = FALSE, $media = 'screen')
	{
		if($ref){
			switch($flag){
			
				case 'css':
					
					$dir = ( $this->isURL($ref) ) ? '' : ( ($cache) ? $this->cache_uri : $this->style_uri );
					
					return '<link type="text/css" rel="stylesheet" href="'.$dir.$ref.'"'.($media ? ' media="'.$media.'"' : '').' />'."\r\n";
				
				break;
		
				case 'js':
					
					$dir = ( $this->isURL($ref) ) ? '' : ( ($cache) ? $this->cache_uri : $this->script_uri );
					
					return '<script type="text/javascript" src="'.$dir.$ref.'" charset="'.$this->CI->config->item('charset').'"></script>'."\r\n";
				
				break;
			
			}
	
		}
	}
	
	protected function _display_js_string($group='main', $wrapTags = TRUE){
		$script = '';
		if(!empty($this->_js_string))
		{
			if( !isset($this->_js_string[$group]) ): // the group you asked for doesn't exist. This should never happen, but better to be safe than sorry.

			log_message('error', "Carabiner: The JavaScript string group named '{$group}' does not exist.");
			return;
	
			endif;
                
			$script = implode(';', $this->_js_string[$group]);
			
			if($this->minify_js && strlen($script)){
				$this->_load('jsmin');

				$script = $this->CI->jsmin->minify($script);
			}
			
			if($wrapTags)
				echo '<script>'.$script.'</script>';
			else
				echo $script;
            }
        }
		
	public function display_js_string($group='main', $wrapTags = TRUE){
		$this->_display_js_string($group, $wrapTags);
	}
	
	public function require_group($group_name)
	{
		if(!isset($this->js[$group_name]) && !isset($this->css[$group_name]) ){
			log_message('warning', "Carabiner: The asset group named '{$group_name}' does not exist");
			return;
		}
		
		if(is_array($group_name))
			$this->groups = array_merge($this->groups, $group_name);
		else
			$this->groups[] = $group_name;	
	}
	
	#Modified to accept array of group names to display
	public function display($flag = 'both', $group_filter = NULL)
	{	

		switch($flag){
			
			case 'JS':
			case 'js':
				$this->_display_js();
                $this->_display_js_string();
			break;
			
			case 'CSS':
			case 'css':
				$this->_display_css();
                $this->_display_css_string();
			break;
			
			case 'both':
				$this->_display_js();
				$this->_display_css();
                $this->_display_js_string();
                $this->_display_css_string();
			break;
			
			default:
				if(!is_array($flag)){
					$flag = array($flag);
				}
				foreach($flag as $group){
					if( isset($this->js[$group]) && ($group_filter == NULL || $group_filter == 'js') ){
						$this->_display_js($group);
						$this->_display_js_string($group);
					}
					
					if( isset($this->css[$group]) && ($group_filter == NULL || $group_filter == 'css') ){
						$this->_display_css($group);
						$this->_display_css_string($group);
					}
				}
			break;
		}
	}
	
	protected function _display_js($group = 'main'){
		if($group == 'main'){
			foreach($this->groups as $group_name){
				parent::_display_js($group_name);
				parent::_display_js_string($group_name);
			}
		}
		parent::_display_js($group);
	}
	
	protected function _display_css($group = 'main'){
		if($group == 'main'){
			foreach($this->groups as $group_name){
				parent::_display_css($group_name);
				parent::_display_css_string($group_name);
			}
		}
		parent::_display_css($group);
	}
}