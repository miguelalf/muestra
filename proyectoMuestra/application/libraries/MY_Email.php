<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Email Class
 *
 * Permits email to be sent using Mail, Sendmail, or SMTP.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/email.html
 */
class MY_Email extends CI_Email {

	var	$debug			= FALSE;
	var	$debug_email	= '';


	/**
	 * Constructor - Sets Email Preferences
	 *
	 * The constructor can be passed an array of config values
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	// --------------------------------------------------------------------

	/**
	 * Send Email
	 *
	 * @access	public
	 * @return	bool
	 */
	/*public function send()
	{
		#for debug config file enable
		if(isset($this->debug) AND $this->debug == TRUE)
		{
			$this->_debug = 'DEBUG_TO: '.$this->_recipients."\n\n";
			$this->_recipients	= array();
			$this->_cc_array	= array();
			$this->_bcc_array	= array();
			
			if(!empty($this->debug_email))
			{
				$this->to($this->debug_email);
				$this->_body = $this->_debug.'DEBUG_MESSAGE: '.$this->_body;
				unset($this->_headers['Bcc'],$this->_headers['Cc']);
			}
			else
			{
				$this->_set_error_message('Debug no recipient defined');
				return FALSE;
			}
		}
		
		if ($this->_replyto_flag == FALSE)
		{
			$this->reply_to($this->_headers['From']);
		}

		if (( ! isset($this->_recipients) AND ! isset($this->_headers['To']))  AND
			( ! isset($this->_bcc_array) AND ! isset($this->_headers['Bcc'])) AND
			( ! isset($this->_headers['Cc'])))
		{
			$this->_set_error_message('lang:email_no_recipients');
			return FALSE;
		}

		$this->_build_headers();

		if ($this->bcc_batch_mode  AND  count($this->_bcc_array) > 0)
		{
			if (count($this->_bcc_array) > $this->bcc_batch_size)
				return $this->batch_bcc_send();
		}

		$this->_build_message();

		if ( ! $this->_spool_email())
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}*/

}
// END MY_Email class

/* End of file MY_Email.php */
/* Location: ./appllication/libraries/MY_Email.php */