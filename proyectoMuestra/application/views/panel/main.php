<h1 class="title page"><?php echo $title; ?></h1>
<div class="white-container">
	<?php if($projects) : ?>
		<h1 class="title">Campañas</h1>
		<table cellspacing="0" cellpadding="4" border="1" class="table table-hovped table-striped table-border">
			<thead>
				<th colspan="2">Acciones</th>
				<th>Nombre</th>
				<th>Autor</th>
				<th>Fecha</th>
				<th>Estado</th>
			</thead>
			<tbody>
				<?php foreach($projects as $project) : ?>
					<tr>
						<td class="text-center">
							<?php if(!$project->accepted) : ?>
								<a href="<?php echo base_url('panel/accept/'.$project->id); ?>" rel="modal-ajax">
							<?php endif; ?>
								<i class="glyphicon glyphicon-ok-circle"></i>
							<?php if(!$project->accepted) : ?></a><?php endif; ?>
						</td>
						<td class="text-center">
							<?php if(!$project->accepted) : ?>
								<a rel="modal-ajax" href="<?php echo base_url('panel/reject/'.$project->id); ?>">
							<?php endif; ?>
								<i class="glyphicon glyphicon-remove-circle"></i>
							<?php if(!$project->accepted) : ?></a><?php endif; ?>
						</td>
						<td>
							<a href="<?php echo base_url('project/view/'.$project->id); ?>" target="_blank">
								<?php echo $project->title; ?>
							</a>
						</td>
						<td><?php echo $project->name; ?></td>
						<td><?php echo date('d/m/Y', $project->create_date); ?></td>
						<td><?php echo ($project->accepted ? ($project->accepted == 2 ? 'Rechazado' : 'Aceptado') : 'Nuevo'); ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>
</div>
<div class="bottom-button-place">
</div>