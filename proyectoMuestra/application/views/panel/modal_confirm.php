<div class="modal-dialog modal-sm">
	<?php echo form_open(); ?>
		<input type="hidden" name="confirm" value="true">
		<div class="modal-content">
			<div class="modal-body">
				<h4 clasS="text-left"><?php echo $text; ?></h4>
			</div>
			<div clasS="modal-footer">
				<div class="text-center">
					<button type="button" class="btn btn-clear" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-yellow"><?php echo (isset($submit_label) ? $submit_label : 'Aceptar'); ?></button>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>