<div class="modal-dialog modal-sm">
<?=form_open(); ?>
  <input type="hidden" name="confirm" value="true">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"><?=$title?></h4>
    </div>
    <div class="modal-body">
      <?php $this->load->view('inc/flash_alerts'); ?>
      <?php if(isset($message)):?>
      	<p><?=$message?></p>
      <?php endif; ?>
      Are you sure you want to do this?      
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary"><?=(isset($submit_label) ? $submit_label : 'Confirm')?></button>
    </div>
  </div>
<?=form_close()?>
</div>

<script>
$('#modal').one('shown.bs.modal', function(){
	$('.modal-footer button').first().focus();	
});
</script>