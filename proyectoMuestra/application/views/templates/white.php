<!DOCTYPE html>
<html lang="<?php echo $this->config->item('language'); ?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title><?php echo (!empty($title) ? $title.' | ' : '').APP_TITLE; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" type="image/png" href="<?php echo base_url(IMG_PATH.'favicon.png'); ?>" />
		<?php $this->load->view('snippets/load_css'); ?>
		<!--[if lt IE 9]>
		<?php $this->carabiner->display('iefix'); ?>
		<![endif]-->
	</head>
	<body class="<?php echo (!empty($bclass) ? $bclass : 'default-body'); ?> default-body">
		<?php $this->load->view($view); ?>
		<?php $this->load->view('snippets/load_js'); ?>
	</body>
</html>