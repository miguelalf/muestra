<h1 class="title page">Contactanos</h1>
<?php echo form_open(); ?>
	<div class="white-container">
		<?php $this->load->view('snippets/flash_alerts'); ?>
		<div class="form-group">
			<label class="form-label" for="name">Nombre <? if(isset($name_error)) : ?>
				<span class="required"><?=$name_error?></span>
			<? endif; ?></label>
			<input id="name" class="form-control" name="u-name" value="<?=set_value('u-name')?>">
		</div>
		<div class="form-group">
			<label class="form-label" for="email">Correo <? if(isset($email_error)) : ?>
				<span class="required"><?=$email_error?></span>
			<? endif; ?></label>
			<input id="email" class="form-control" name="u-email" value="<?=set_value('u-email',@$this->session->email?:'')?>">
		</div>
		<div class="form-group">
			<label class="form-label" for="message">Mensaje <? if(isset($message_error)) : ?>
				<span class="required"><?=$message_error?></span>
			<? endif; ?></label>
			<div class="textControl">
				<textarea rows="5" id="message" class="form-control no-resize limitedText" name="u-message"><?=set_value('u-message')?></textarea>
				<div class="letter-control">
					<? $letters_limit = 300; ?>
					<? $remaining_letters = $letters_limit - strlen(set_value('u-message')); ?>
					<span class="remaining-letters"><?=$remaining_letters?></span>/<span class="letters-limit"><?=$letters_limit?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-button-place clearfix">
		<button type="submit" class="btn btn-black pull-right">Enviar</button>
	</div>
<?php echo form_close(); ?>