<?php $this->load->view('snippets/header'); ?>

<?php if(!empty($top_project)) : ?>
	<section class="section-gray">
		<div class="container">
			<div class="row">
				<div class="col-sm-offset-1 col-sm-10">
					<div class="row top-project">
						<h1 class="title title-uppercase text-center top hidden-xs">Campaña destacada</h1>
						<h1 class="title page top visible-xs">Campaña destacada</h1>
						<div class="col-sm-8 col-md-6">
							<div class="visible-xs">
								<a href="<?php echo base_url('ver-proyecto/'.$top_project->id); ?>">
									<div class="thumbnail">
										<img src="<?php echo base_url(PUBLIC_PATH.'project/'.$top_project->image); ?>">
									</div>
								</a>
							</div>
							<h1 class="title sub-title title-uppercase"><a href="<?=base_url('ver-proyecto/'.$top_project->id)?>"><?php echo $top_project->title; ?></a></h1>
							<p><small>Por <a class="important-anchor" href="<?=base_url('profile/'.$top_project->user_id)?>" target="_blank"><?php echo $top_project->name; ?></a></small></p>
							<p><?php echo $top_project->presentation; ?></p>
							<div class="progres-bar"></div>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?= ($top_project->p_amount * 100) / $top_project->p_goal; ?>%">
									<span class="sr-only"></span>
								</div>
							</div>
							<div class="row text-center mb10">
								<div class="col-xs-4 p0">
									<p class="cipher border">$<?php echo number_format($top_project->p_goal,2); ?></p>
									<p><small>Meta</small></p>
								</div>
								<div class="col-xs-4 p0">
									<p class="cipher border"><?php echo ($top_project->p_amount * 100) / $top_project->p_goal; ?>%</p>
									<p><small>Recaudado</small></p>
								</div>
								<div class="col-xs-4 p0">
									<p class="cipher"><?php echo $top_project->backers->count ?></p>
									<p><small>Donadores</small></p>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-6 hidden-xs">
							<div class="image position-relative">
								<div class="image-container hovereffect">
									<?php if($top_project->image) : ?>
										<a href="<?=base_url('ver-proyecto/'.$top_project->id)?>">
											<img src="<?php echo base_url(PUBLIC_PATH.'project/'.$top_project->image); ?>">
										</a>
									<?php else: ?>
										<div style="height: 190px"></div>
									<?php endif; ?>
									<div class="overlay">
										<h2><?php echo ($top_project->accepted ? $this->timeflow->days_left($top_project->start_date,$top_project->n_days) :'Aún no empieza'); ?></h2>
										<a class="btn btn-yellow" href="<?=base_url('ver-proyecto/'.$top_project->id)?>">Ver campaña</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
	
<div class="white-section clearfix" id="section_target">
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-1 col-sm-10">
				<a class="btn btn-clear pull-right grid hidden-xs" href="<?php echo base_url('proyectos'); ?>">Ver todas</a>
				<h1 class="title title-uppercase grid hidden-xs">+ CAMPAÑAS DE AYUDA</h1>
				<div class="ml30">
					<h1 class="title page visible-xs">CAMPAÑAS DE AYUDA</h1>
				</div>
				<div class="projects-grid">
					<?php if(!empty($projects)) : ?>
						<?php foreach($projects as $project):?>
							<?php $data['project'] = $project; ?>
							<?php $this->load->view('snippets/project_box', $data); ?>
						<?php endforeach; ?>
					<?php else : ?>
						<?php $this->load->view('snippets/project_box', array('project' => $dummy_project)); ?>
					<?php endif; ?>
				</div>
				<div class="text-center visible-xs">
					<a class="btn btn-clear mb20" href="<?php echo base_url('projects'); ?>">Ver todas</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view('snippets/window'); ?>