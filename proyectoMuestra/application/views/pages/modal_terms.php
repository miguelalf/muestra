<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
			<h3 class="title"><?php echo $title; ?></h3>
		</div>		
		<div class="modal-body">
			<?php ?>
		</div>
	</div>
</div>