<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
			<h3 class="title"><?php echo $title; ?></h3>
		</div>		
		<div class="modal-body">

			<p>El proyecto Boteadora tiene 4 funciones básicas para el apoyo a proyectos de emprendimiento ó de iniciativa social:</p>
			
			<ol>
				<li>
					<p><strong>Donación Social.</strong></p>
					<p>Apoyo a Iniciativas sociales, asociaciones civiles y proyectos sociales, la donación se da a la asociación para cubrir las necesidades descritas 
					en el proyecto, la asociación o iniciativa civil nos entregará evidencias de las mejoras con tu donación.</p>
				</li>
				<li>
					<p><strong>Donación por Recompensa.</strong></p>
					<p>¡Próximamente tendremos esta opción! Este tipo de donación puede aplicar en el ámbito cultural y proyectos de emprendimiento, donde tu donación tendrá una recompensa directamente por 
					la persona que está pidiendo el apoyo.</p>
				</li>
				<li>
					<p><strong>Inversión por proyecto productivo.</strong></p>
					<p>¡Próximamente tendremos esta opción! Síguenos en nuestra página de Facebook y sé el primero en enterarte cuando arranque esta etapa.</p>
				</li>
				<li>
					<p><strong>Préstamos para iniciar un proyecto.</strong></p>
					<p>¡Próximamente tendremos esta opción! Síguenos en nuestra página de Facebook y sé el primero en enterarte cuando arranque esta etapa.</p>
				</li>
			</ol>

		</div>
	</div>
</div>