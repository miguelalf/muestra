<h1 class="page title"><?php echo $title; ?></h1>
<div class="white-container">

	<div class="text-center mb30"><h1 class="title">Terminos y condiciones</h1></div>

	<p>Al hacer uso de boteadora.com queda sujeta a los Términos y Condiciones de los
	servicios prestados por <strong>www.boteadora.com</strong> Por lo que no están sujetos a modificación
	alguna , por lo que se entiende que son aceptadas por los Usuarios de boteadora.com, se
	reserva el derecho a modificar los Términos y Condiciones de este sitio de internet.</p>

	<p><strong>Boteadora.com</strong> de centro de desarrollo social y humano s.c. es un servicio en la web para
	que los usuarios puedan realizar donaciones colectivas de dinero o “crowdfunding” para los
	Proyectos grupales o individuales ya sea cultural, tecnológico, científico, ecológico, editorial
	proyectos de emprendedores y de iniciativa social . lo anterior Para obtener los recursos
	necesarios para su realización, también hay una sección de aportaciones para Ayuda
	comunitaria.</p>

	<p>Para poder hacer <strong>uso de los servicios</strong> de <strong>boteadora.com</strong> es necesario 
	que la persona responsable del proyecto tenga la capacidad legal para contratar como persona física o
	como representante de una empresa, se registre con sus datos personales en
	<strong>boteadora.com</strong> donde manifiesta que esta de acuerdo y que entiende que el único servicio
	que presta boteadora.com es una plataforma web, para que los usuarios puedan aportar
	dinero para los proyectos de su interés boteadora.com se reserva el derecho de solicitar a
	los Usuarios algún comprobante a efectos de corroborar los datos personales, se reserva
	suspender temporal o definitivamente a aquellos Usuarios cuyos datos no hayan podido ser
	confirmados. <strong>Boteadora.com</strong> se reserva el derecho de rechazar cualquier solicitud de
	inscripción o de cancelar una inscripción previamente aceptada, sin que esté obligado a
	comunicar o exponer las razones de su decisión y sin que ello genere algún derecho a
	indemnización a favor del Usuario. <strong>Los Métodos de Pago</strong> en las cuales los usuarios de
	<strong>boteadora.com</strong> podrán realizar sus aportaciones voluntarias en favor de determinado
	Proyecto es a través de paypal con tarjeta de crédito o debito. En efectivo en las tiendas
	Extra. O transferencia electrónica. Los Montos Aportados a favor de un Proyecto será
	considerado sin la comisión que cobran los diferentes métodos de pago de acuerdo a lo
	señalado en su Sitio o en su tienda.</p>

	<p><strong>Boteadora.com</strong> tendrá cinco días hábiles para <strong>revisión del proyecto</strong> a partir del momento
	de su recepción, podemos solicitarle llenar información adicional y pedir hablar con el
	responsable. si el proyecto es aceptado por <strong>boteadora.com</strong>, le enviaremos un correo
	electrónico notificándole que su proyecto a sido aceptado esperando del usuario en
	<strong>boteadora.com</strong> un acuse de recibo desde el correo registrado. Dicha decisión, será tomada
	de acuerdo a la revisión del proyecto por <strong>boteadora.com</strong>, por lo que en caso de ser
	rechazada no existirá algún tipo de responsabilidad para <strong>boteadora.com</strong></p>

	<p>Para subir el proyecto a el sitio de <strong>boteadora.com</strong> el responsable del proyecto deberá
	entregar un video y hasta diez imágenes que se utilizaran para su presentación asi como
	toda la información del proyecto. El responsable del proyecto deberá <strong>difundirlo</strong> con la
	finalidad de lograr obtener el mayor numero de participantes</p>

	<p><strong>Boteadora.com</strong> dará seguimiento a cada uno de los proyectos que a sido apoyado por los
	usuarios con su donación, En el caso que haya recibido fondos por parte de <strong>los usuarios</strong>,
	el responsable del proyecto se encontrará obligado a utilizar los mismos exclusivamente en
	la realización del Proyecto. <strong>El Creador</strong> será el único responsable de la correcta utilización de
	los fondos para la realización del Proyecto, sin que pudiera imputársele a
	<strong>boteadora.com</strong> algún tipo de responsabilidad por el incumplimiento del proyecto.
	<strong>Boteadora.com</strong> publicitara en el mismo proyecto los avances o terminación del proyecto. En
	caso de que el proyecto por parte del responsable del proyecto por alguna razón no
	imputable a <strong>boteadora.com</strong> desista del proyecto., el responsable del servicio acepta que
	perderá el derecho a solicitar dichos montos, aceptando que boteadora.com disponga de
	ellos conforme convenga a sus intereses. <strong>El usuario del servicio</strong> también acepta y
	reconoce que no tendrá derecho alguno sobre el Proyecto donde realice sus aportaciones.
	<strong>boteadora.com</strong> dispondrá de los fondos recaudados para obras sociales o de caridad.</p>

	<p><strong>El Pago de Comisión a Boteadora.com</strong> Que se cobrará al responsable del proyecto es del
	6% del total del Monto Aportado por parte de los usuarios más el impuesto al valor agregado
	correspondiente, por concepto de comisión. <strong>Boteadora.com</strong> no es responsable en el
	cumplimiento de las obligaciones fiscales o establecidas por la ley vigente que se generen
	para el responsable del proyecto. <strong>Boteadora.com</strong> no será responsable por cualquier otro
	daño y/o perjuicio que se causen por las acciones u omisiones del responsable del proyecto.</p>

	<p><strong>Boteadora.com</strong> no tiene participación alguna en el desarrollo del Proyecto o Iniciativa
	Creativa, por lo que no será responsable por el efectivo cumplimiento de su realización.
	este acuerdo con el responsable del proyecto no crea con <strong>boteadora.com</strong> ninguna relación
	laboral, de franquicia, o sociedad</p>

	<p>La inscripción y publicación del Proyecto o Iniciativa Creativa en el Sitio no tendrá costo. los
	métodos de pago se cobrara comisiones .</p>

	<p>Los usuarios al realizar sus aportaciones, están de acuerdo en cualquiera de los Métodos
	de Pago usados les será descontados el pago de comisiones por usar sus servicios
	<ul>
		<li>Aportación con tarjeta de Crédito/Débito</li>
		<li>Aportación en efectivo por Extra, Farmacias venbides</li>
		<li>Transferencia electrónica.</li>
	</ul>
	La comisiónes cobradas serán de .04% y una cuota fija de 2 pesos hasta 8
	pesos más iva</p>

	<p>Para más información, puede consultar la página de Openpay:</p>
	
	<p><a href="http://www.openpay.mx/costos.html">openpay.mx</a></p>
	
	<p>Los montos de dichas comisiones, serán descontadas de los montos aportados por
	los usuarios de <strong>boteadora.com</strong></p>

	<p><strong>Comisión de Boteadora.com:</strong> Aplica una comisión del 4% mas iva, misma que el usario
	acepta y está de acuerdo que sea descontada directamente de la aportación por parte de
	<strong>boteadora.com</strong></p>

	<div class="text-center mv30"><h1 class="title">Aviso de privacidad</h1></div>

	<p>De acuerdo a lo Previsto en la <strong>“Ley Federal de Protección de Datos Personales”</strong>, Declara
	Centro de Desarrollo Social y Humano S.C. en el servicio de <strong>boteadora.com</strong> ser una
	empresa legalmente constituida de conformidad con las leyes mexicanas y como
	responsable del tratamiento de sus datos personales, hace de su conocimiento que la
	información de nuestros clientes es tratada de forma estrictamente confidencial por lo que al
	proporcionar sus datos personales (tales como: nombre completo, dirección, Registro
	Federal de Contribuyentes, teléfono) estos serán utilizados única y exclusivamente para los
	siguientes fines: Información y Prestación de Servicios así como la actualización de la base
	de datos.</p>

	<p>Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en
	el país, por ello le informamos que usted tiene en todo momento los derechos de rectificar,
	cancelar u oponerse al tratamiento que le damos a sus datos personales; derecho que
	podrá hacer valer a través del Área de Privacidad encargada de la seguridad de datos
	personales en el teléfono 656 6662913 ó por medio de su correo electrónico
	informacion@cdsh.tv</p>

	<p>Los datos personales proporcionados por el usuario formarán parte de un archivo que
	contendrá su perfil. El usuario puede acceder o modificar su perfil en cualquier momento
	utilizando su número de usuario o enviándonos un correo a informacion@cdsh.tv
	Esta declaración de Confidencialidad / Privacidad está sujeta a los términos y condiciones
	del sitio, lo cual constituye un acuerdo legal entre el usuario y Centro de Desarrollo Social y
	Humano S.C. Si el usuario utiliza el sitio web de Centro de Desarrollo Social y Humano S.C.
	significa que ha leído, entendido y acordado los términos antes expuestos.</p>

	<p>Domicilio de Centro de Desarrollo Social y Humano S.C.
	<ul>
		<li>Calle Diamante 1715</li>
		<li>Fraccionamiento Bonanza</li>
		<li>Cd. Juárez Chihuahua, México</li>
		<li>C.P. 32000</li>
		<li>Tel. 6662913</li>
	</ul></p>

	<div class="text-center mv30"><h1 class="title">Jurisdicción y Ley Aplicable</h1></div>

	<p>Este acuerdo estará regido en todos sus puntos por las leyes vigentes en la República
	Mexicana, en particular respecto de mensajes de datos, contratación electrónica y comercio
	electrónico se regirá por lo dispuesto por la legislación federal respectiva.
	Para la interpretación, cumplimiento y ejecución del presente contrato, las partes
	expresamente se someten a la jurisdicción de los tribunales competentes de la Ciudad de
	Juárez Chihuahua renunciando en consecuencia a cualquier fuero que en razón de su
	domicilio presente o futuro pudiera corresponderles.</p>

	<p class="text-center">ATENTAMENTE</p>

	<p class="text-center">Centro de Desarrollo social y Humano S.C.</p>
</div>
<div class="bottom-button-place"></div>