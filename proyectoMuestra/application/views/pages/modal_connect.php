<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
			<h3 class="title">Contactanos!</h3>
		</div>		
		<div class="modal-body">
			<?php echo form_open(); ?>
				<?php $this->load->view('snippets/flash_alerts'); ?>
				<div class="form-group">
					<label class="form-label" for="name">Nombre <?php if(isset($name_error)) : ?>
						<span class="required"><?php echo $name_error; ?></span>
					<?php endif; ?></label>
					<input id="name" class="form-control" name="u-name" value="<?php echo set_value('u-name'); ?>">
				</div>
				<div class="form-group">
					<label class="form-label" for="email">Correo <?php if(isset($email_error)) : ?>
						<span class="required"><?php echo $email_error; ?></span>
					<?php endif; ?></label>
					<input id="email" class="form-control" name="u-email" value="<?php echo set_value('u-email',@$this->session->email?:''); ?>">
				</div>
				<div class="form-group">
					<label class="form-label" for="message">Mensaje <?php if(isset($message_error)) : ?>
						<span class="required"><?php echo $message_error; ?></span>
					<?php endif; ?></label>
					<div class="textControl">
						<textarea rows="5" id="message" class="form-control no-resize limitedText" name="u-message"><?php echo set_value('u-message'); ?></textarea>
						<div class="letter-control">
							<?php $letters_limit = 300; ?>
							<?php $remaining_letters = $letters_limit - strlen(set_value('u-message')); ?>
							<span class="remaining-letters"><?php echo $remaining_letters; ?></span>/<span class="letters-limit"><?php echo $letters_limit; ?></span>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-clear btn-fat">Enviar</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

<script src="assets/js/textcontrol.js"></script>