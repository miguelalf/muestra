<section class="section-gray">
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-6">
				<h1 class="title page">Iniciar sesión</h1>
				<div class="white-container">
					<?php echo form_open(); ?>
						<?php $this->load->view('snippets/flash_alerts'); ?>
						<div class="form-group">
							<label class="form-label" for="email">Correo electrónico <?php if(isset($email_error)) : ?>
								<span class="required"><?php echo $email_error; ?></span>
							<?php endif; ?></label>
							<input id="email" class="form-control input-regular" name="email" value="<?php echo set_value('email'); ?>">
						</div>
						<div class="form-group">
							<label class="form-label" for="password">Contraseña <?php if(isset($pass_error)) : ?>
								<span class="required"><?php echo $pass_error; ?></span>
							<?php endif; ?></label>
							<input type="password" id="password" class="form-control input-regular" name="password" value="<?php echo set_value('password'); ?>">
							<a href="<?php echo site_url('recuperar-contrasena'); ?>">¿Olvidaste tu contraseña?</a>
						</div>
						<div class="form-group">
							<div class="checkbox">
								<label class="form-label"><input type="checkbox" name="remember" value="1"> Recordarme</label>
							</div>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-yellow btn-fat btn-shadow">Iniciar sesión</button>
						</div>
					<?php echo form_close(); ?>
				</div>
				<div class="bottom-button-place clearfix text-center">
					<h1 class="title">¿No tienes cuenta?</h1>
					<a class="btn btn-gray" href="<?php echo base_url('registro'); ?>">Registrate aqui</a>
				</div>
			</div>
		</div>
	</div>
</div>