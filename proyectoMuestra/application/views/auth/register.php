<section class="section-gray">
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-6">
				<h1 class="title page">Registro</h1>
				<?php echo form_open(); ?>
					<div class="white-container">
						<?php $this->load->view('snippets/flash_alerts'); ?>
						<div class="form-group">
							<label class="form-label control-label" for="name">Nombre <?php if(isset($name_error)) : ?>
								<span class="required"><?php echo $name_error; ?></span>
							<?php endif; ?></label>
							<input id="name" class="form-control input-regular" name="name" value="<?php echo set_value('name'); ?>">
						</div>
						<div class="form-group">
							<label class="form-label control-label" for="email">Correo electrónico <?php if(isset($email_error)) : ?>
								<span class="required"><?php echo $email_error; ?></span>
							<?php endif; ?></label>
							<input id="email" class="form-control input-regular" name="email" value="<?php echo set_value('email'); ?>">
						</div>
						<div class="form-group">
							<label class="form-label control-label" for="password">Contraseña <?php if(isset($pass_error)) : ?>
								<span class="required"><?php echo $pass_error; ?></span>
							<?php endif; ?></label>
							<input type="password" id="password" class="form-control input-regular" name="password">
						</div>
						<div class="form-group">
							<label class="form-label control-label" for="passconf">Confirmar contraseña <?php if(isset($pass_conf_error)) : ?>
								<span class="required"><?=$pass_conf_error?></span>
							<?php endif; ?></label>
							<input type="password" id="passconf" class="form-control input-regular" name="passconf">
						</div>
						<div class="form-group">
							<p>Al registrarte aceptas nuestros <a href="<?php echo base_url('terminos'); ?>" target="_blank">términos de uso y condiciones</a></p>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-yellow btn-fat btn-shadow">Crear cuenta</button>
						</div>
					</div>
					<div class="bottom-button-place clearfix text-center">
						<h1 class="title">¿Ya tienes cuenta?</h1>
						<a class="btn btn-gray" href="<?php echo base_url('login'); ?>">Inicia sesión aqui</a>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>