<div class="modal-dialog modal-login modal-sm">
	<?php echo form_open('auth/forgotten_password'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
				<h3 class="title"><?php echo $title; ?></h3>
			</div>
			<div class="modal-body">
				<?php $this->load->view('snippets/flash_alerts'); ?>
				<div class="form-group">
					<label class="form-label" for="email">Correo electrónico <?php if(isset($email_error)) : ?>
					<span class="required"><?php echo $email_error; ?></span>
					<?php endif; ?></label>
					<input id="email" class="form-control input-regular" name="email" value="<?php echo set_value('email'); ?>">
					<small><i>*Te enviaremos un correo con los pasos a seguir para reestablecer tu contraseña</i></small>
				</div>
				<div class="text-center form-group clearfix">
					<button class="btn btn-clear btn-fat" type="submit">Recuperar</button>
				</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<p class="m0">¿Ya recordaste tu contraseña? <br>
						<a href="<?php echo base_url('login'); ?>" rel="modal-ajax">Inicia sesión aqui</a>
					</p>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>