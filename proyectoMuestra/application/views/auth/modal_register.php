<div class="modal-dialog modal-register modal-sm">
	<?php echo form_open('auth/register'); ?>
		<input type="hidden" name="confirm" value="true">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
				<h3 class="title"><?php echo $title; ?></h3>
			</div>
			<div class="modal-body">
				<?php $this->load->view('snippets/flash_alerts'); ?>
				<div class="form-group">
					<label class="form-label label-control" for="name">Nombre <?php if(isset($name_error)) : ?>
						<span class="required"><?php echo $name_error; ?></span>
					<?php endif; ?></label>
					<input id="name" class="form-control input-regular" name="name" value="<?php echo set_value('name'); ?>">
				</div>
				<div class="form-group">
					<label class="form-label label-control" for="email">Correo electrónico <?php if(isset($email_error)) : ?>
						<span class="required"><?=$email_error?></span>
					<?php endif; ?></label>
					<input id="email" class="form-control input-regular" name="email" value="<?php echo set_value('email'); ?>">
				</div>
				<div class="form-group">
					<label class="form-label label-control" for="password">Contraseña <?php if(isset($pass_error)) : ?>
						<span class="required"><?php echo $pass_error; ?></span>
					<?php endif; ?></label>
					<input type="password" id="password" class="form-control input-regular" name="password">
				</div>
				<div class="form-group">
					<label class="form-label label-control" for="passconf">Confirmar contraseña <?php if(isset($pass_conf_error)) : ?>
						<span class="required"><?php echo $pass_conf_error; ?></span>
					<?php endif; ?></label>
					<input type="password" id="passconf" class="form-control input-regular" name="passconf">
				</div>
				<div class="form-group">
					<p>Al registrarte aceptas nuestros <a href="<?php echo base_url('terminos'); ?>" target="_blank">términos de uso y condiciones</a></p>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-clear btn-fat">Crear cuenta</button>
				</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<p class="m0">¿Ya tienes cuenta? <a href="<?php echo base_url('auth/login'); ?>" rel="modal-ajax">Inicia sesión</a></p>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>