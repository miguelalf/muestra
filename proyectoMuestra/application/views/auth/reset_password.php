<section class="section-gray">
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-6">
				<h1 class="title page">Restablece tu contraseña</h1>
				<div class="white-container">
					<?php echo form_open(); ?>
						<div class="form-group">
							<label class="control-label form-label" for="pass">Nueva contraseña <?php if(isset($pass_error)) : ?>
								<span class="required"><?php echo $pass_error; ?></span>
							<?php endif; ?></label>
							<input id="pass" class="form-control input-regular" name="pass" type="password">
						</div>
						<div class="form-group">
							<label class="control-label form-label" for="conf">Confirmar contraseña <?php if(isset($conf_error)) : ?>
								<span class="required"><?php echo $conf_error; ?></span>
							<?php endif; ?></label>
							<input id="conf" class="form-control input-regular" name="conf" type="password">
						</div>
						<div class="text-center">
							<button class="btn btn-clear btn-fat" type="submit">Guardar</button>
						</div>
					<?php echo form_close(); ?>
				</div>
				<div class="bottom-button-place clearfix text-center">
					<h1 class="title">¿Ya tienes cuenta?</h1>
					<a class="btn btn-yellow btn-shadow" href="<?php echo base_url('login'); ?>">Inicia sesión aqui</a>
				</div>
			</div>
		</div>
	</div>
</section>