<div class="modal-dialog modal-login modal-sm">
	<?php echo form_open('auth/login'); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
				<h3 class="title"><?php echo $title; ?></h3>
			</div>
			<div class="modal-body">
				<?php $this->load->view('snippets/flash_alerts'); ?>
				<div class="form-group">
					<label class="form-label label-control" for="email">Correo electrónico <?php if(isset($email_error)) : ?>
						<span class="required"><?php echo $email_error; ?></span>
					<?php endif; ?></label>
					<input id="email" class="form-control input-regular" name="email" value="<?php echo set_value('email'); ?>">
				</div>
				<div class="form-group">
					<label class="form-label label-control" for="password">Contraseña <?php if(isset($pass_error)) : ?>
						<span class="required"><?php echo $pass_error; ?></span>
					<?php endif; ?></label>
					<input type="password" id="password" class="form-control input-regular" name="password" value="<?php echo set_value('password'); ?>">
					<a href="<?php echo base_url('recuperar-contrasena'); ?>" rel="modal-ajax">¿Olvidaste tu contraseña?</a>
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label class="form-label"><input type="checkbox" name="remember" value="1"> Recordarme</label>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-clear btn-fat">Iniciar sesión</button>
				</div>
			</div>
			<div class="modal-footer">
				<div class="text-center">
					<p class="m0">¿No tienes cuenta? <a href="<?php echo base_url('registro'); ?>" rel="modal-ajax">Resgistrate aqui</a></p>
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>