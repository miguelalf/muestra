<section class="section-gray">
	<div class="container">
		<div class="row">
			<div class="col-sm-offset-3 col-sm-6">
				<h1 class="title page">¿Olvidaste tu contraseña?</h1>
				<div class="white-container">
					<?php echo form_open(); ?>
						<div class="form-group">
							<label class="form-label" for="email">Correo electrónico <?php if(isset($email_error)) : ?>
							<span class="required"><?php echo $email_error; ?></span>
							<?php endif; ?></label>
							<input id="email" class="form-control input-regular" name="email" value="<?php echo set_value('email'); ?>">
							<small><i>*Te enviaremos un correo con los pasos a seguir para reestablecer tu contraseña</i></small>
						</div>
						<div class="form-group clearfix">
							<button class="btn btn-yellow btn-shadow btn-fat" type="submit">Recuperar</button>
						</div>
					<?php echo form_close(); ?>
				</div>
				<div class="bottom-button-place clearfix text-center">
					<h1 class="title">¿Ya recordaste tu contraseña?</h1>
					<a class="btn btn-gray" href="<?php echo base_url('login'); ?>">Inicia sesión aqui</a>
				</div>
			</div>
		</div>
	</div>
</section>