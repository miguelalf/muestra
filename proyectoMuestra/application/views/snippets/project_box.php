<div class="project-box">
	<div class="image-container">
		<div class="hovereffect">
			<?php if($project->image) : ?>
				<a href="<?=base_url('ver-proyecto/'.$project->id)?>">
					<img src="<?php echo base_url(PUBLIC_PATH.'project/'.$project->image); ?>">
				</a>
			<?php else: ?>
				<div style="height: 190px"></div>
			<?php endif; ?>
			<div class="overlay">
				<h2><?php echo ($project->accepted ? $this->timeflow->days_left($project->start_date,$project->n_days) :'Aún no empieza'); ?></h2>
				<a class="btn btn-yellow" href="<?=base_url('ver-proyecto/'.$project->id)?>">Ver campaña</a>
			</div>
		</div>
	</div>
	<div class="case">
		<div class="title-container">
			<h1 class="title"><a href="<?=base_url('ver-proyecto/'.$project->id)?>"><?=$project->title?></a></h1>
			<p class="description"><?=$project->presentation?></p>
		</div>
		<div class="progres-bar"></div>
		<div class="progress">
			<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?= ($project->p_amount * 100) / $project->p_goal; ?>%">
				<span class="sr-only"></span>
			</div>
		</div>
		<div class="row text-center ma0 mb10">
			<div class="col-xs-4 p0">
				<p class="cipher border">$<?php echo number_format($project->p_goal,2); ?></p>
				<p><small>Meta</small></p>
			</div>
			<div class="col-xs-4 p0">
				<p class="cipher border"><?php echo ($project->p_amount * 100) / $project->p_goal; ?>%</p>
				<p><small>Recaudado</small></p>
			</div>
			<div class="col-xs-4 p0">
				<p class="cipher"><?php echo $project->backers->count ?></p>
				<p><small>Donadores</small></p>
			</div>
		</div>
		<?php if(@$in_view) : ?>
			<div class="text-center mb10">
				<a class="btn btn-clear" href="<?php echo base_url('editar-proyecto/'.$project->id); ?>"><i class="fa fa-pencil-square-o pr10"></i> Editar campaña</a>
			</div>
		<?php endif; ?>
	</div>
</div>