<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-12 text-center mt20">
				<a href="<?php echo base_url(); ?>">
					<img class="boteadora-footer" src="<?php echo base_url(IMG_PATH.'logo_boteadora_blanco.png'); ?>" alt="boteadora">
				</a>
				<span>Es un proyecto de</span>
				<a href="http://cdsh.tv/" target="_blank">
					<img class="boteadora-footer" src="<?php echo base_url(IMG_PATH.'logo_cdsh.png'); ?>" alt="CDSH">
				</a>
			</div>
			<div class="col-xs-6 visible-xs">
				<ul class="clearfix">
					<li><a href="<?php echo base_url('how-support'); ?>" rel="modal-ajax">¿Cómo apoyar?</a></li>
					<li><a href="<?php echo base_url('proyectos'); ?>">Campañas</a></li>
					<li><a href="<?php echo base_url('us'); ?>" rel="modal-ajax">Nosotros</a></li>
					<li><a href="<?php echo base_url('contacto'); ?>">Contacto</a></li>
					<li><a href="<?php echo base_url('terminos'); ?>">Terminos</a></li>
				</ul>
				<span class="pull-right">&copy; Boteadora 2016</span>
			</div>
		</div>
		<hr class="hidden-xs">
		<div class="row hidden-xs">
			<div class="col-sm-12">
				<span class="pull-right">&copy; Boteadora 2016</span>
				<ul class="clearfix">
					<li><a href="<?php echo base_url(); ?>">Inicio</a></li>
					<li><a href="<?php echo base_url('how-support'); ?>" rel="modal-ajax">¿Cómo apoyar?</a></li>
					<li><a href="<?php echo base_url('proyectos'); ?>">Campañas</a></li>
					<li><a href="<?php echo base_url('us'); ?>" rel="modal-ajax">Nosotros</a></li>
					<li><a href="<?php echo base_url('contacto'); ?>" rel="modal-ajax">Contacto</a></li>
					<li><a href="<?php echo base_url('terminos'); ?>">Terminos</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>