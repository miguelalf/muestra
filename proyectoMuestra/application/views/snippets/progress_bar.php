<div class="progres-bar"></div>
<div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?= ($project->p_amount * 100) / $project->p_goal; ?>%">
		<span class="sr-only"></span>
	</div>
</div>
<div class="row text-center mb10">
	<div class="col-md-4 p0">
		<p class="cipher border">$<?=$project->p_goal?></p>
		<p><small>Meta</small></p>
	</div>
	<div class="col-md-4 p0">
		<p class="cipher border"><?= ($project->p_amount * 100) / $project->p_goal; ?>%</p>
		<p><small>Recaudado</small></p>
	</div>
	<div class="col-md-4 p0">
		<p class="cipher"><?= $project->backers->count ?></p>
		<p><small>Donadores</small></p>
	</div>
</div>