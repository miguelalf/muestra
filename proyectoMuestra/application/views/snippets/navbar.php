<nav class="navbar navbar-inverse navbar-fixed-top navbar-yellow" role="navigation" id="navbar-main">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarOptions">
			<span class="sr-only">Mostrar menú</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?php echo base_url(); ?>"><i class="icon-tdj"></i><img class="boteadora-header" src="<?php echo base_url(IMG_PATH.'logo_boteadora_negro.png'); ?>"></a>
    </div>
	<div class="visible-xs-block xs-options">
		<div class="collapse navbar-collapse menu-collapse" id="navbarOptions">
			<ul class="nav navbar-nav nav-options-list mv0">
				<?php if($this->session->user_id) : ?>
					<li><a href="<?php echo base_url('usuario'); ?>">Tu perfil</a></li>
					<li><a href="<?php echo base_url('empezar'); ?>">Crear nueva campaña</a></li>
					<li><a href="<?php echo base_url('mis-proyectos/'.$this->session->user_id); ?>">Tus campañas</a></li>
				<?php else: ?>
					<li><a href="<?php echo base_url('login'); ?>">Iniciar sesión</a></li>
				<?php endif; ?>
				<li><a href="<?php echo base_url('how-support'); ?>" rel="modal-ajax">¿Cómo apoyar?</a></li>
				<li><a href="<?php echo base_url('proyectos'); ?>">Campañas</a></li>
				<li><a href="<?php echo base_url('us'); ?>" rel="modal-ajax">Nosotros</a></li>
				<li><a href="<?php echo base_url('contacto'); ?>">Contacto</a></li>
				<?php if($this->session->user_id) : ?>
					<li><a href="<?php echo base_url('auth/logout'); ?>"><i class="icon-signout"></i>Cerrar sesión</a></li>
				<?php endif; ?>
			</ul>
		</div>
	</div>
    <div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo base_url('how-support'); ?>" rel="modal-ajax">¿Cómo apoyar?</a></li>
			<li><a href="<?php echo base_url('proyectos'); ?>">Campañas</a></li>
			<li><a href="<?php echo base_url('us'); ?>" rel="modal-ajax">Nosotros</a></li>
			<li><a href="<?php echo base_url('contacto'); ?>" rel="modal-ajax">Contacto</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<?php if(!$this->session->user_id) : ?>
				<li><a class="options-button" href="<?php echo base_url('login'); ?>" rel="modal-ajax">Iniciar sesión</a></li>
			<?php else : ?>
				<li>
					<div class="dropdown options-menu">
						<a class="logout dropdown-toggle options-menu-inside options-button" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="icon-place mr30"><?php echo strstr($this->session->name,' ',true); ?></span><i class="fa fa-cog " aria-hidden="true"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-right with-pointer" aria-labelledby="dropdownMenu1">
							<li><a href="<?php echo base_url('usuario'); ?>">Tu perfil</a></li>
							<li><a href="<?php echo base_url('empezar'); ?>">Crear nueva campaña</a></li>
							<li><a href="<?php echo base_url('mis-proyectos/'.$this->session->user_id); ?>">Tus campañas</a></li>
							<?php if(1 == $this->session->group): ?><li><a href="<?php echo base_url('panel'); ?>">Panel de administración</a></li><?php endif; ?>
							<li><a href="<?php echo base_url('auth/logout'); ?>"><i class="icon-signout"></i> Cerrar sesión</a></li>
						</ul>
					</div>
				</li>
			<?php endif; ?>
		</ul>
		<a href="https://www.facebook.com/boteadora/" target="_blank">
			<img class="pull-right mt10 mr20" src="<?php echo base_url(IMG_PATH.'fb_blanco.png'); ?>" style="max-width:30px">
		</a>
	</div>
</nav>

<?php $this->carabiner->js_string(<<<'EOT'

	$('.options-menu').on('hide.bs.dropdown', function () {
		$(this).find('.fa-cog').removeClass('fa-rotating-animation');
	});
	
	$('.options-menu').on('show.bs.dropdown', function () {
		$(this).find('.fa-cog').addClass('fa-rotating-animation');
	});

EOT
,'jquery'); ?>