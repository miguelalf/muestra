<?
$alert = array(
	'success'	=> $this->session->flashdata('alert-success').@$alert_success,
	'warning'	=> $this->session->flashdata('alert-warning').@$alert_warning,
	'info'		=> $this->session->flashdata('alert-info')	 .@$alert_info,
	'danger'	=> $this->session->flashdata('alert-danger') .@$alert_danger,
);


$container = isset($container) && $container;
$size = isset($size) ? 'alert-'.$size : '';
foreach($alert as $k=>$v)
{
	if(!empty($v)){
		echo '
		<div class="alert alert-'.$k.' '.$size.'">'.
			($container ? '<div class="container">' : '').'
				<button type="button" class="close" data-dismiss="alert">
				  <span aria-hidden="true">&times;</span>
				  <span class="sr-only">cerrar</span>
				</button>
				'.$v.
			($container ? '</div>' : '').'
		</div>';
	}
}?>