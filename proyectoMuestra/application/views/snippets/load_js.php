<?
$this->carabiner->js(array(
	array('jquery.form.min.js'),
	array('jquery.fileinput.js'),
	array('summernote.min.js'),
	array('summernote-es-ES.min.js'),
	array('parallax.min.js'),
	array('ajax-helper.js'),
	array(in_url('project/view') ? 'facebook.js' : ''),
	array('script.js?v=0.0.1'),
	array('textcontrol.js?v=0.0.1')
));

$this->carabiner->display('jquery');
$this->carabiner->display('bootstrap','js');
$this->carabiner->display('js');

?>
