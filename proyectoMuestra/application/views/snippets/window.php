<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url(IMG_PATH.'banner_footer.jpg'); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 text-center parallax-content-height">
						<h1 class="title rounded text-white text-uppercase">¿Aún no sabes si ayudar?</h1>
						<p class="text-white">¡Ayúdanos a mejorar la ciudad para todos! </p>
						<a href="<?php echo base_url('how-works'); ?>" rel="modal-ajax" class="btn btn-yellow mb10">Como funciona</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>