<section class="section-space">
	<img class="stars-left" src="<?php echo base_url(IMG_PATH.'estrellas_izquierda.png'); ?>">
	<img class="stars-right" src="<?php echo base_url(IMG_PATH.'estrellas_derecha.png'); ?>">
	<div class="container mt50">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">
				<img class="mv10" src="<?php echo base_url(IMG_PATH.'loguito.png'); ?>">
				<h1>Juntos llegamos más lejos</h1>
				<p>¡Ayúdanos a mejorar la ciudad para todos! Entra en la descripción de cada proyecto y conoce a fondo todas las necesidades que están buscando cubrir, podrás apoyar causas sociales, culturales ó personales.</p>
			</div>
		</div>
	</div>
	<img class="world rotating" src="<?php echo base_url(IMG_PATH.'mundo.png'); ?>">
	<div class="jump">
		<a href="#" data-target="section_target">Apoyar <br><i class="fa fa-chevron-down"></i></a>
	</div>
</section>

<?php $this->carabiner->js_string(<<<'EOT'

	$('.jump').on('click', 'a', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('html, body').animate({ scrollTop: $('#'+$(this).attr('data-target')).offset().top-50 }, 300, 'linear');
	});

EOT
,'jquery'); ?>