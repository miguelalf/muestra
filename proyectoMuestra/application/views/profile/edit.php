<div class="container">
	<div class="row">
		<div class="col-sm-offset-2 col-sm-8">
			<?php echo form_open('',array('enctype' => 'multipart/form-data')); ?>
				<h1 class="title page">Editar perfil</h1>
				<div class="white-container">
					<?php $this->load->view('snippets/flash_alerts'); ?>
					<div class="position-relative">
						<div class="left-chunk">
							<div class="form-group">
								<div class="image-container large">
									<img id="imagePreview" src="<?php echo base_url($user->image ? PUBLIC_PATH.'profile/'.$user->image : IMG_PATH.'default-image.jpg'); ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="btn-group" style="width: 100%;">
									<div class="btn btn-gray btn-file btn-fat">
										<i class="fa fa-picture-o"></i>
										<span class="btn-file-text">Subir <span class="trap">imagen</span></span>
										<input type="file" id="imageInput" name="image">
									</div>
								</div>
							</div>
						</div>
						<div class="right-chunk">
							<label class="form-label control-label" for="biography">Biografía <span class="small-quote"><small><i>Pequeña descripción tuya</i></small></span></label>
							<div class="form-group">
								<textarea rows="4" id="biography" class="form-control no-resize input-regular" name="biography"><?php echo @$user->biography; ?></textarea>
							</div>
							<label class="form-label control-label" for="location">Ubicación</label>
							<div class="form-group">
								<input id="location" class="form-control input-regular" name="location" value="<?php echo @$user->location; ?>">
							</div>
							<label class="form-label control-label" for="account">Cuenta bancaria <span class="small-quote"><small><i>Obligatorio para campañas</i></small></span></label>
							<div class="form-group">
								<input type="number" id="account" class="form-control input-regular" name="account" value="<?php echo @$user->account; ?>">
							</div>
							<label class="form-label control-label" for="webs">Sitios web</label>
							<input hidden id="webs" name="webs" value='<?php echo @$user->webs; ?>'>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-9 pr0">
										<input id="new-web" class="form-control input-regular">
									</div>
									<div clasS="col-sm-3 p0 text-center">
										<button type="button" id="add-web" class="btn btn-gray"><span class="add-trap"><span>Agregar</span></span></button>
									</div>
								</div>
							</div>
							<div id="urls-web" class="form-group">
								<? if($user->webs) : ?>
									<? $webs = explode(',', $user->webs); array_shift($webs); ?>
									<? foreach($webs as $web) : ?>
										<div class="web-list-item">
											<a href="<?='http://'.$web?>" target="_blank"><?=$web?></a><a href="#" class="remove-link" rel="<?=$web?>">&times;</a>
										</div>
									<? endforeach; ?>
								<? endif; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="bottom-button-place clearfix">
					<button type="submit" class="btn btn-black pull-right">Guardar</button>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<?php $this->load->view('snippets/window'); ?>