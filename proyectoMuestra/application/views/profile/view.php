<div class="container">
	<div class="row">
		<div class="col-sm-offset-2 col-sm-8">
			<h1 class="title page"><?=$user->user_id == $this->session->user_id ? 'tu perfil' : 'perfil'?></h1>
			<div class="white-container">
				<div class="position-relative clearfix">
					<div class="left-chunk">
						<div class="profile-image">
							<div class="form-group">
								<div class="image-container large image-center">
									<img id="imagePreview" src="<?=base_url($user->image ? PUBLIC_PATH.'profile/'.$user->image : IMG_PATH.'default-image.jpg')?>">
								</div>
							</div>
						</div>
						<?php if($this->session->user_id == $user->user_id) : ?>
							<a href="<?=base_url('profile/edit')?>" class="btn btn-gray btn-fat"><i class="fa fa-pencil-square-o pr10"></i> Editar <span class="trap">perfil</span></a>
						<?php endif; ?>
					</div>
					<div class="right-chunk">
						<h1 class="title profile"><?=$user->name?></h1>
						<p><strong>Miembro desde : </strong> <?=date('M.  Y',$user->created_on)?></p>
						<p><strong>Biografía</strong></p>
						<p><?=$user->biography?:'<i class="small-quote">Vacío por el momento</i>'?></p>
						<p><strong>Ubicación : </strong></p>
						<p><?=$user->location?:'<i class="small-quote">Desconocida</i>'?></p>
						<? if($user->webs) : ?>
							<p><strong>Sitios web : </strong></p>
							<? $webs = explode(',', $user->webs); array_shift($webs); ?>
							<? foreach($webs as $web) : ?>
								<div class="web-list-item">
									<a href="<?='http://'.$web?>" class="full" target="_blank"><?=$web?></a>
								</div>
							<? endforeach; ?>
						<? endif; ?>
					</div>
				</div>
			</div>
			<div class="bottom-button-place clearfix"></div>
		</div>
	</div>
</div>
<?php $this->load->view('snippets/window'); ?>