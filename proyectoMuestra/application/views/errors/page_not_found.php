<div class="row">
	<div class="col-xs-12">
		<div class="text-center">
			<h1>Pagina no encontrada</h1>
			<p>Ruta desconocida o se carece de permisos para acceder</p>
			<a onClick="history.back(-1);" class="btn btn-yellow">Regresar</a>
		</div>
	</div>
</div>