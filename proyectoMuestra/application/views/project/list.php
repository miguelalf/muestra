<h1 class="title page">Todas las campañas</h1>
<? if($projects) : ?>
	<div class="projects-grid">
		<? foreach($projects as $project) : ?>					
			<?php $data['project'] = $project; ?>
			<?php $this->load->view('snippets/project_box', $data); ?>
		<? endforeach; ?>
		<div class="bottom-button-place clearfix">
			<div class="text-center">
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
<? else : ?>
	<div class="white-container">
		<div class="text-center">
			<h1 class="title">Sin campañas por mostrar</h1>
			<? if($this->session->user_id == $user_id) : ?>
				<a class="btn btn-clear mt20 mb10" href="<?=site_url('start')?>">Comienza una aqui</a>
			<? endif; ?>
		</div>
	</div>
	<div class="bottom-button-place clearfix"></div>
	<?php $this->load->view('snippets/window'); ?>
<? endif; ?>
