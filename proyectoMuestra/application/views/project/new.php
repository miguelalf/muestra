<?php echo form_open('',array('enctype' => 'multipart/form-data')); ?>
	<h1 class="title page">Información básica<p class="inside">Apariencia</p></h1>
	<div class="white-container">
		<?php $this->load->view('snippets/flash_alerts'); ?>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label" for="title">Nombre <?php echo isset($title_error) ? '<span class="required">'.$title_error.'</span>' : ''; ?></label>
					<div class="textControl">
						<input id="title" class="form-control input-regular limitedText" name="title" value="<?php echo set_value('title'); ?>">
						<div class="letter-control regular">
							<?php $letters_limit = 100; ?>
							<?php $remaining_letters = $letters_limit - strlen(set_value('title')); ?>
							<span class="remaining-letters"><?php echo $remaining_letters; ?></span>/<span class="letters-limit"><?php echo $letters_limit; ?></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label" for="description">Descripción <?php echo isset($description_error) ? '<span class="required">'.$description_error.'</span>' : ''; ?></label>
					<div class="textControl">
						<textarea id="description" class="form-control input-regular no-resize limitedText" name="description" rows="4"><?php echo set_value('description'); ?></textarea>
						<div class="letter-control regular">
							<?php $letters_limit = 150; ?>
							<?php $remaining_letters = $letters_limit - strlen(set_value('description')); ?>
							<span class="remaining-letters"><?php echo $remaining_letters; ?></span>/<span class="letters-limit"><?php echo $letters_limit; ?></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label" for="category">Categoría <?php echo isset($category_error) ? '<span class="required">'.$category_error.'</span>' : ''?></label>
					<select id="category" class="form-control input-regular" name="category">
						<option></option>
						<?php foreach($categories as $category) : ?>
							<option  value="<?php echo $category->id; ?>" <?php echo set_select('category',$category->id); ?>><?php echo ucfirst($category->slug); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label" for="imageInputCustom">Imagen principal del proyecto <?php echo isset($image_error) ? '<span class="required">'.$image_error.'</span>' : ''; ?></label>
					<input type="file" id="imageInputCustom" name="image">	
				</div>
			</div>
		</div>
	</div>
	<h1 class="title page">Información básica<p class="inside">Contenido</p></h1>
	<div class="white-container">
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label control-label" for="goal">Meta <?php echo isset($goal_error) ? '<span class="required">'.$goal_error.'</span>' : ''; ?></label>
					<div class="input-group">
						<div class="input-group-addon">$</div>
						<input id="goal" class="form-control input-regular" name="goal" value="<?php echo set_value('goal'); ?>">
						 <div class="input-group-addon">MXN</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label control-label" for="days">Tiempo <?php echo isset($days_error) ? '<span class="required">'.$days_error.'</span>' : ''; ?></label>
					<div class="input-group">
						<select id="days" class="form-control input-regular" name="days">
							<?php for($n = 1; $n*15 <= 90; $n++) : ?>
								<?php $s = $n*15; ?>
								<option value="<?php echo $s; ?>" <?php echo set_select('days',$s); ?>><?php echo $s; ?></option>
							<?php endfor; ?>
						</select>
						<div class="input-group-addon">Días</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="form-label control-label" for="content">Contenido</label>
			<textarea id="content" class="form-control input-regular" name="content" rows="6"><?=set_value('content')?></textarea>
		</div>
	</div>
	<div class="bottom-button-place clearfix">
		<button type="submit" class="btn btn-black pull-right">Empezar</button>
	</div>
<?php echo form_close(); ?>

<?php $this->carabiner->js_string(<<<'EOT'

	$('form').on('submit', function(e){
		if(isNaN($('#goal').val())){
			e.preventDefault();
			e.stopImmediatePropagation();
			if(!$('#goal').closest('.form-group').hasClass('has-error')){
				$('#goal').focus().closest('.form-group').addClass('has-error');
				$('label[for="goal"]').append('<span class="required">Solo numeros</span>');
			} else $('#goal').focus();
			//$('body').scrollspy({ target: '#goal' })
			$('html, body').animate({ scrollTop: $('#goal').offset().top-300 }, 300, 'linear');
		}
	});
	
	$('.form-control').bind('input', function() { 
		if($(this).closest('.form-group').hasClass('has-error')){
			$(this).closest('.form-group').removeClass('has-error').find('.required').remove();
		}
	});
 
EOT
,'jquery'); ?>