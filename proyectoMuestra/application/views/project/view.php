<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-<?php echo $col; ?> col-md-offset-<?php echo $col_offset; ?>">
			<div class="top-project text-center">
				<h1 class="title"><?php echo $project->title; ?></h1>
				<p class="position-relative">
					<small>Creado por <a class="important-anchor" href="<?=base_url('usuario/'.$project->user_id)?>">
						<?php echo $project->name; ?>
					</a></small>
					<a href="<?=base_url('usuario/'.$project->user_id)?>">
						<img class="nano-thumb" src="<?php echo base_url(PUBLIC_PATH.'profile/'.$project->user_image); ?>">
					</a>
				</p>
			</div>
			<div class="col-sm-8">
				<div class="project-banner">
					<?php if($project->image) : ?>
						<div class="image-preview">
							<img class="project-image" src="<?=base_url(PUBLIC_PATH.'project/'.$project->image)?>">
						</div>
					<?php else : ?>
						<div class="no-banner block-table position-relative">
							<div class="block-table-middle">
								<i><span class="small-quote">No banner</span></i>
							</div>
							<img id="imagePreview" class="project-image">
						</div>
					<?php endif; ?>
				</div>
				<div class="mt20 project">
					<div class="col-xs-12 visible-xs pa0">
						<div class="white-container">
							<div class="text-center">
								<h1 class="title-block">Recaudado</h1>
								<p class="title"><b><big>$<?=number_format($project->p_amount,2)?></big></b></p>
								<div class="progres-bar"></div>
								<div class="progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?= ($project->p_amount * 100) / $project->p_goal; ?>%">
										<span class="sr-only"></span>
									</div>
								</div>
								<div class="row text-center mb10">
									<div class="col-xs-4 p0">
										<p class="cipher border">$<?php echo number_format($project->p_goal,0); ?></p>
										<p><small>Meta</small></p>
									</div>
									<div class="col-xs-4 p0">
										<p class="cipher border"><?php echo ($project->p_amount * 100) / $project->p_goal; ?>%</p>
										<p><small>Recaudado</small></p>
									</div>
									<div class="col-xs-4 p0">
										<p class="cipher"><?php echo $project->backers->count; ?></p>
										<p><small>Donadores</small></p>
									</div>
								</div>
								<center>
									<?php if($active) : ?>
										<a class="btn btn-yellow" href="<?php echo base_url('contribucion/'.$project->id); ?>">Apoyar</a>
									<?php elseif($project->accepted) : ?>
										<a class="btn btn-clear" disabled="disabled">La campaña acabó</a>
									<?php else : ?>
										<a class="btn btn-clear" disabled="disabled">Aún no empieza</a>
									<?php endif; ?>
								</center>
							</div>
						</div>
					</div>
					<h1 class="title"><?php echo $project->title; ?></h1>
					<p><i><?php echo $project->presentation?></i></p>
					<hr class="text-pipe">
					<?php echo html_entity_decode($project->content)?>
					<hr class="text-pipe">
				</div>
			</div>
			<div class="col-sm-4 mb30">
				<div class="box-affix affix-top hidden-xs" data-spy="affix">
					<div class="white-container">
						<div class="text-center">
							<h1 class="title-block">Recaudado</h1>
							<p class="title"><b><big>$<?=number_format($project->p_amount,2)?></big></b></p>
							<div class="progres-bar"></div>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?= ($project->p_amount * 100) / $project->p_goal; ?>%">
									<span class="sr-only"></span>
								</div>
							</div>
							<div class="row text-center mb10">
								<div class="col-xs-4 p0">
									<p class="cipher border">$<?php echo number_format($project->p_goal,0); ?></p>
									<p><small>Meta</small></p>
								</div>
								<div class="col-xs-4 p0">
									<p class="cipher border"><?php echo ($project->p_amount * 100) / $project->p_goal; ?>%</p>
									<p><small>Recaudado</small></p>
								</div>
								<div class="col-xs-4 p0">
									<p class="cipher"><?php echo $project->backers->count; ?></p>
									<p><small>Donadores</small></p>
								</div>
							</div>
							<center>
								<?php if($active) : ?>
									<a class="btn btn-yellow" href="<?php echo base_url('contribucion/'.$project->id); ?>">Apoyar</a>
								<?php elseif($project->accepted) : ?>
									<a class="btn btn-clear" disabled="disabled">La campaña acabó</a>
								<?php else : ?>
									<a class="btn btn-clear" disabled="disabled">Aún no empieza</a>
								<?php endif; ?>
							</center>
						</div>
					</div>
					<center class="pa20">
						<a href="<?php echo base_url('projects/report/'.$project->id); ?>" class="btn btn-gray	btn-fat" rel="modal-ajax">Reportar esta campaña</a>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>