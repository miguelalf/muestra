<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<?php echo form_open('projects/report/'.$project_id); ?>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
				<h3 class="title"><?php echo $title; ?></h3>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="form-label label-control" for="name">Nombre <?php if(@$name_error) : ?>
						<span class="required"><?php echo $name_error; ?></span>
					<?php endif; ?></label>
					<input id="name" class="form-control input-regular" name="name" value="<?php echo set_value('name',@$user->name); ?>" placeholder="Nombre Apellido">
				</div>
				<div class="form-group">
					<label class="form-label label-control" for="email">Correo <?php if(@$email_error) : ?>
						<span class="required"><?php echo $email_error; ?></span>
					<?php endif; ?></label>
					<input id="email" class="form-control input-regular" name="email" type="email" value="<?php echo set_value('email',@$user->email); ?>" placeholder="usuario@dominio.com">
				</div>
				<div class="form-group">
					<label class="form-label label-control" for="comment">Motivo <?php if(@$comment_error) : ?>
						<span class="required"><?php echo $comment_error; ?></span>
					<?php endif; ?></label>
					<textarea rows="3" id="comment" class="form-control input-regular no-resize" name="comment" placeholder="El motivo del reporte es ..."><?php echo set_value('comment'); ?></textarea>
				</div>
				<hr>
				<div class="text-center">
					<button type="submit" class="btn btn-clear btn-fat">Reportar campaña</button>
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>