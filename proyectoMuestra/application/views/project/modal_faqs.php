<div class="modal-dialog modal-sm">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><?=$title?></h4>
		</div>
		<div class="modal-body">
			<p class="text-center"><?=$content?></p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Regresar</button>
		</div>
	</div>
</div>