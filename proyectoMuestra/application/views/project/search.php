<h1 class="title page">Todas las campañas</h1>
<form method="post" accept-charset="utf-8">
	<div class="row">
		<div class="col-xs-7 col-md-offset-1 mb20">
			<div class="input-group">
				<span class="input-group-btn">
					<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				</span>
				<input class="form-control" name="search" value="<?php echo set_value('search'); ?>" placeholder="Buscar...">
			</div>
		</div>
		<div class="col-xs-5 col-md-3">
			<select id="category" class="form-control" name="category">
				<option value="0">-- Categorías --</option>
				<?php foreach($categories as $category) : ?>
					<option value="<?php echo $category->id; ?>" <?php echo set_select('category',$category->id); ?>><?php echo ucfirst($category->slug); ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
</form>
<?php if($projects) : ?>
	<div class="projects-grid">
		<?php foreach($projects as $project) : ?>					
			<?php $data['project'] = $project; ?>
			<?php $this->load->view('snippets/project_box', $data); ?>
		<?php endforeach; ?>
		<div class="bottom-button-place clearfix">
			<div class="text-center">
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
<?php else : ?>
	<div class="white-container">
		<div class="text-center">
			<h1 class="title">Sin campañas por mostrar</h1>
			<a class="btn btn-clear mt20 mb10" href="<?php echo site_url(); ?>">Volver a inicio</a>
		</div>
	</div>
	<div class="bottom-button-place clearfix"></div>
	<?php $this->load->view('snippets/window'); ?>
<?php endif; ?>

<?php $this->carabiner->js_string(<<<'EOT'
	
	$('#category').on('change', function(e){
		$('form').submit();
	});

EOT
,'jquery'); ?>