<?php echo form_open('',array('enctype' => 'multipart/form-data')); ?>
	<h1 class="title page">Apariencia</h1>
	<div class="white-container">
		<?php $this->load->view('snippets/flash_alerts'); ?>
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label control-label" for="title">Nombre <?php echo isset($title_error)?'<span class="required">'.$title_error.'</span>':''; ?></label>
					<div class="textControl">
						<input id="title" class="form-control input-regular limitedText" name="title" value="<?=set_value('title',$project->title)?>">
						<div class="letter-control">
							<?php $letters_limit = 100; ?>
							<?php $remaining_letters = $letters_limit - strlen(set_value('title',$project->title)); ?>
							<span class="remaining-letters"><?=$remaining_letters?></span>/<span class="letters-limit"><?=$letters_limit?></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label control-label" for="description">Descripción <?=isset($description_error)?'<span class="required">'.$description_error.'</span>':''?></label>
					<div class="textControl">
						<textarea id="description" class="form-control input-regular no-resize limitedText" name="description" rows="3"><?=set_value('description',$project->presentation)?></textarea>
						<div class="letter-control">
							<? $letters_limit = 150; ?>
							<? $remaining_letters = $letters_limit - strlen(set_value('description',$project->presentation)); ?>
							<span class="remaining-letters"><?=$remaining_letters?></span>/<span class="letters-limit"><?=$letters_limit?></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="form-label control-label" for="category">Categoría <?=isset($category_error)?'<span class="required">'.$category_error.'</span>':''?></label>
					<select id="category" class="form-control" name="category">
						<option></option>
						<? foreach($categories as $category) : ?>
							<option  value="<?=$category->id?>" <?=set_select('category',$category->id)?> <?=$project->category==$category->id?'selected':''?>><?=ucfirst($category->slug)?></option>
						<? endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label control-label" for="imageInput">Imagen principal del proyecto</label>
					<input type="file" id="imageInputCustom" name="image">
					<input class="hidden" id="imageName" value="<?php echo $project->image; ?>">
				</div>
			</div>
		</div>
	</div>
	<h1 class="title page">Contenido</h1>
	<div class="white-container">
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label control-label" for="goal">Meta <?php echo isset($goal_error) ? '<span class="required">'.$goal_error.'</span>' : ''; ?></label>
					<div class="input-group">
						<div class="input-group-addon">$</div>
						<input id="goal" class="form-control input-regular" name="goal" value="<?php echo set_value('goal',$project->p_goal); ?>">
						 <div class="input-group-addon">MXN</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label class="form-label control-label" for="days">Tiempo <?php echo isset($days_error) ? '<span class="required">'.$days_error.'</span>' : ''; ?></label>
					<div class="input-group">
						<select id="days" class="form-control input-regular" name="days">
							<?php for($n = 1; $n*15 <= 90; $n++) : ?>
								<?php $s = $n*15; ?>
								<option value="<?php echo $s; ?>" <?php echo set_select('days',$s); echo ($project->n_days*1 == $s ? 'selected' : ''); ?>><?php echo $s; ?></option>
							<?php endfor; ?>
						</select>
						<div class="input-group-addon">Días</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="form-label control-label" for="content">Contenido</label>
			<textarea id="content" class="form-control no-resize" name="content" rows="6"><?=set_value('content',html_entity_decode($project->content))?></textarea>
		</div>
	</div>
	<div class="bottom-button-place clearfix">
		<button type="submit" class="btn btn-black pull-right">Guardar</button>
		<a class="btn btn-gray pull-right mr10" href="<?php echo base_url('project/view/'.$project->id); ?>">Ver campaña</a>
	</div>
<?php echo form_close(); ?>