<style type="text/css" media="print">
	.btn.btn-black{
		display:none!important;
	}
	.top-log{
		padding:20px!important;
		background-color:#ffe450!important;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="white-macro text-center">
				<div class="ma10 mh30 top-log" style="padding:20px;background-color:#ffe450">
					<img src="<?php echo base_url(IMG_PATH.'logo_boteadora_blanco.png'); ?>" style="max-width:300px">
				</div>
				<div class="row mb20">
					<div class="col-xs-6 pr0">
						<div class="cipher border">
							<h2><small>Total a pagar</small></h2>
							<h1 class="text-center">$ <?php echo number_format($charge->amount,2); ?> MXN</h1>
							<h2 class="text-center"><small> + $8 pesos por comisión</small></h2>
						</div>
					</div>
					<div class="col-xs-6 pl0">
						<div class="mt30">
							<img src="<?php echo base_url(IMG_PATH.'corazon.png'); ?>" style="max-width:120px">
						</div>
					</div>
				</div>
				<img src="<?php echo $barcode; ?>">
				<h3 class="ma10"><small><?php echo $charge->reference; ?></small></h3>
				<div class="pa10 pt30">
					<img src="<?php echo base_url(IMG_PATH.'horizontal.gif'); ?>" style="max-width:420px">
				</div>
				<a class="btn btn-black mr10" onClick="window.print()">Imprimir</a>
				<a class="btn btn-gray" href="<?php echo base_url('gracias/'.$project); ?>">Terminar</a>
			</div>
		</div>
	</div>
</div>