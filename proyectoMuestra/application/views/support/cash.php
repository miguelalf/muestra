<?php echo form_open('',array('id' => 'cashForm')); ?>
	<h1 class="title page">Información de facturación</h1>
	<div class="white-container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-0 col-xs-offset-1 col-xs-10">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Nombre</label>
							<input class="form-control input-regular" name="firstName" placeholder="Nombre">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Apellido</label>
							<input class="form-control input-regular" name="lastName" placeholder="Apellido">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Ciudad</label>
							<input class="form-control input-regular" name="city" placeholder="Ciudad">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Dirección</label>
							<input class="form-control input-regular" name="address" placeholder="Dirección">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Código postal</label>
							<input class="form-control input-regular" name="zip" placeholder="Código postal">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Teléfono<span class="required"></span></label>
							<input class="form-control input-regular" name="phone" placeholder="Teléfono">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2">
				<div class="form-group">
					<label class="form-label">Resumen</label>
					<div class="thumbnail no-frame">
						<img class="img-responsive" src="<?php echo base_url(PUBLIC_PATH.'project/'.$project->image); ?>">
					</div>
					<p><span class="mr10">Proyecto:</span><strong><?php echo $project->title; ?></strong></p>
					<p><span class="mr10">Aportación:</span><strong>$<?php echo  number_format($amount/100, 2); ?> MXN</strong></p>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-button-place clearfix">
		<button type="submit" class="btn btn-black pull-right">Aceptar</button>
		<a class="btn btn-gray pull-right mr10" href="<?php echo base_url('donation/amount/'.$project->id); ?>">Regresar</a>
	</div>
<?php echo form_close(); ?>

<?php $this->carabiner->js_string(<<<'EOT'

	$('#cashForm').on('submit',function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		var $form = $(this);
		$form.find('button').prop('disabled', true);
		if($form.find('[name="phone"]').val()){
			$form.get(0).submit();
		}else{
			$form.find('.required').text('Campo obligatorio').closest('.form-group').addClass('has-error');
			$form.find('button').prop('disabled', false);
		}
	});

EOT
,'jquery'); ?>