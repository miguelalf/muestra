<?php echo form_open(); ?>
<h1 class="title page">Apoyando a "<?php echo $project->title; ?>"</h1>
<div class="white-container">
	<?php $this->load->view('snippets/flash_alerts'); ?>
	<div class="row">
		<div class="col-sm-8">
			<div class="form-group <?=isset($amount_error)?'has-error':''?>">
				<label class="form-label" for="amount">Contribución <?php echo isset($amount_error)?'<span class="required">'.$amount_error.'</span>':''; ?></label>
				<div class="input-group">
					<div class="input-group-addon">$</div>
					<input id="amount" class="form-control input-regular" name="amount">
					<div class="input-group-addon">MXN</div>
				</div>
			</div>
			<div class="form-group">
				<label class="form-label" for="method">Método de pago</label>
				<div class="form-group">
					<label><input type="radio" name="method" value="1" <?php echo  set_radio('method', '1', TRUE); ?>><span class="ml10">Tarjeta de crédito o débito <img src="<?=base_url(IMG_PATH.'tarjetas_logos.png');?>" style="max-width:100px" class="ml20"></span></label>
				</div>
				<div class="form-group">
					<label><input type="radio" name="method" value="2" <?php echo  set_radio('method', '2'); ?>><span class="ml10">Efectivo en tienda </span></label>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2">
			<div class="form-group">
				<label class="form-label">Resumen</label>
				<div class="thumbnail no-frame">
					<img class="img-responsive" src="<?php echo base_url(PUBLIC_PATH.'project/'.$project->image); ?>">
				</div>
				<p><span class="mr10">Proyecto:</span><strong><?php echo $project->title; ?></strong></p>
			</div>
		</div>
	</div>
</div>
<div class="bottom-button-place clearfix">
	<button type="submit" class="btn btn-black pull-right">Aceptar</button>
	<a class="btn btn-gray pull-right mr10" href="" onClick="history.back(-1);">Regresar</a>
</div>
<?php echo form_close(); ?>

<?php $this->carabiner->js_string(<<<'EOT'
	
	$('form').on('submit', function(e){
		if(isNaN($('#amount').val())){
			e.preventDefault();
			e.stopImmediatePropagation();
			if(!$('#amount').closest('.form-group').hasClass('has-error')){
				$('#amount').focus().closest('.form-group').addClass('has-error');
				$('label[for="amount"]').append('<span class="required">Solo numeros</span>');
			} else $('#amount').focus();
			$('html, body').animate({ scrollTop: $('#amount').offset().top-300 }, 300, 'linear');
		}
	});
 
EOT
,'jquery'); ?>