<?php echo form_open('',array('id' => 'payment-form')); ?>
	<h1 class="title page">Información de cobro</h1>
	<div class="white-container">
		<input type="hidden" name="token_id" id="token_id">
		<div id="alerts">
			<?php $this->load->view('snippets/flash_alerts'); ?>
		</div>
		<div class="form-group">
			<label class="form-label control-label">Nombre del titular</label>
			<input class="form-control input-regular" placeholder="Nombre del titular" autocomplete="off" data-openpay-card="holder_name"/>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-xs-8 col-md-6">
					<div class="form-group">
						<label class="form-label control-label">Número de tarjeta</label>
						<input class="form-control input-regular" placeholder="Número de tarjeta" autocomplete="off" data-openpay-card="card_number"/>
					</div>
				</div>
				<div class="col-xs-4 col-md-2">
					<label class="form-label control-label">CVN</label>
					<input class="form-control input-regular" placeholder="CVN" size="4" autocomplete="off" data-openpay-card="cvv2">
				</div>
				<div class="col-xs-12 col-md-4">
					<label class="form-label control-label">Fecha de expiración</label>
					<div class="row">
						<div class="col-xs-4 col-md-6">
							<select class="form-control input-regular" data-openpay-card="expiration_month">
								<?php for($n = 1; $n <= 12; $n++) : ?>
									<option value="<?php echo str_pad($n, 2, 0, STR_PAD_LEFT); ?>"><?php echo str_pad($n, 2, 0, STR_PAD_LEFT); ?></option>
								<?php endfor; ?>
							</select>
						</div>
						<div class="col-xs-4 col-md-6">
							<select class="form-control input-regular" data-openpay-card="expiration_year">
								<?php $c_year = substr(date('Y'), 2); ?>
								<?php for($n = $c_year; $n <= $c_year+20; $n++) : ?>
									<option value="<?php echo $n; ?>"><?php echo $n; ?></option>
								<?php endfor; ?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<h1 class="title page">Información de facturación</h1>
	<div class="white-container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-0 col-xs-offset-1 col-xs-10">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Nombre<span class="required"></span></label>
							<input class="form-control input-regular" name="firstName" placeholder="Nombre" value="<?php echo set_value('firstName'); ?>">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Apellido</label>
							<input class="form-control input-regular" name="lastName" placeholder="Apellido" value="<?php echo set_value('lastName'); ?>">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Correo<span class="required"></span></label>
							<input class="form-control input-regular" name="email" placeholder="Correo electrónico" value="<?php echo set_value('email',@$user->email); ?>">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="form-label">Teléfono</label>
							<input class="form-control input-regular" name="phone" placeholder="Teléfono" value="<?php echo set_value('phone'); ?>">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-sm-offset-0 col-xs-8 col-xs-offset-2">
				<div class="form-group">
					<label class="form-label">Resumen</label>
					<div class="thumbnail no-frame">
						<img class="img-responsive" src="<?php echo base_url(PUBLIC_PATH.'project/'.$project->image); ?>">
					</div>
					<p><span class="mr10">Proyecto:</span><strong><?php echo $project->title; ?></strong></p>
					<p><span class="mr10">Aportación:</span><strong>$<?php echo  number_format($amount/100, 2); ?> MXN</strong></p>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-button-place clearfix">
		<button type="submit" class="btn btn-black pull-right" id="pay-button">Aceptar y pagar</button>
		<a class="btn btn-gray pull-right mr10" href="<?php echo base_url('contribucion/'.$project->id); ?>">Regresar</a>
	</div>
<?php echo form_close(); ?>