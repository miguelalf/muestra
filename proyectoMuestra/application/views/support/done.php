<h1 class="title page">¡Gracias por tu donación!</h1>
<div class="white-container">
	<img class="img-responsive pig" src="<?php echo base_url(IMG_PATH.'cerdi.png'); ?>">
	<div class="text-center">
		<h1 class="title">Con tu ayuda, este y muchos proyectos más se harán realidad</h1>
		<a href="<?php echo base_url(); ?>" class="btn btn-yellow ma20">Volver a inicio</a>
	</div>
</div>
<div class="bottom-button-place">
</div>