<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('redirect_js')){
	function redirect_js($uri=''){
		if (!preg_match('#^(https?:)?//#i', $uri)){
			$uri = site_url($uri);
		}
		die('<script type="text/javascript">window.location= "'.$uri.'"</script>');
	}
}

if (!function_exists('scape_modal')){
	function scape_modal(){
		die('<script type="text/javascript">$("#modal").hide();$(".modal-backdrop").hide();location.reload();</script>');
	}
}

if (!function_exists('in_url')){
	function in_url($uri='view'){
		return (strpos(current_url(),$uri) !== false) ? true : false;
	}
}

?>