<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

##
# REDIRECTS
#
	if ( ! function_exists('redirect_flash'))
	{
		function redirect_flash($url='',$type='',$message='')
		{
			$ci =&get_instance();
			$ci->session->set_flashdata($type,$message);
			
			redirect($url);
		}
	}
	if ( ! function_exists('redirect_error_flash'))
	{
		function redirect_error_flash($url='',$message='')
		{
			redirect_flash($url,'alert-danger',$message);
		}
	}
	if ( ! function_exists('redirect_success_flash'))
	{
		function redirect_success_flash($url='',$message='')
		{
			redirect_flash($url,'alert-success',$message);
		}
	}
	if ( ! function_exists('redirect_warning_flash'))
	{
		function redirect_warning_flash($url='',$message='')
		{
			redirect_flash($url,'alert-warning',$message);
		}
	}
	if ( ! function_exists('redirect_info_flash'))
	{
		function redirect_info_flash($url='',$message='')
		{
			redirect_flash($url,'alert-info',$message);
		}
	}