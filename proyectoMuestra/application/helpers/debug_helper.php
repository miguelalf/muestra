<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

#debugea una variable
if ( ! function_exists('debug'))
{
	function debug($var)
	{
		ob_start();
		var_dump($var);
		$dump = ob_get_contents();
		ob_end_clean();
		return '<pre>'.$dump.'</pre>';
	}
}
#imprime el debugueo de una variable
if ( ! function_exists('ed'))
{	
	function ed($var)
	{
		echo debug($var);
	}
}
#termina el codigo con el debugueo de una variable
if ( ! function_exists('xd'))
{	
	function xd($var)
	{
		die(debug($var));
	}
}