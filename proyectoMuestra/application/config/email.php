<?
/**
* CODEIGNITER EMAIL CONFIG
*
* Original codeigniter config vars
*/
$config['protocol']		= 'mail';
$config['charset']		= 'utf-8';
$config['mailtype']		= 'html';
$config['wordwrap']		= TRUE;

/**
* DEBUG EMAIL
*
* When enable (debug=TRUE) all sending mails will go only to debug_email
*/
$config['debug']		= (ENVIRONMENT === 'development');
$config['debug_email']	= 'developer@koalaworkshop.com.mx';