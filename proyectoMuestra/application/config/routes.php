<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'boteadora';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// pages
$route['contacto'] = $route['connect'] = 'boteadora/connect';
$route['terminos'] = $route['terms'] = 'boteadora/terms';
$route['how-support'] = 'boteadora/how_support';
$route['how-works'] = 'boteadora/how_works';
$route['us'] = 'boteadora/us';

// auth
$route['login'] = 'auth/login';
$route['registro'] = $route['register'] = 'auth/register';
$route['salir'] = $route['logout'] = 'auth/logout';
$route['recuperar-contrasena'] = 'auth/forgotten_password';
$route['restablecer-contrasena/(:any)'] = 'auth/reset_password/$1';

// users
$route['usuario'] = $route['profile'] = 'profile/view';
$route['usuario/(:num)'] = $route['profile/(:num)'] = 'profile/view/$1';

// project
$route['proyectos'] = 'projects';
$route['proyectos/(:num)'] = $route['projects/(:num)'] = 'projects/index/$1';
$route['empezar'] = $route['start'] = 'projects/new_project';
$route['ver-proyecto/(:num)'] = $route['project/view/(:num)'] = 'projects/view/$1';
$route['editar-proyecto/(:num)'] = $route['project/settings/(:num)'] = 'projects/project_settings/$1';
$route['mis-proyectos/(:num)'] = 'projects/all/$1';

// suport
$route['contribucion/(:num)'] = 'donation/amount/$1';
$route['pagar/(:num)'] = 'donation/checkout/$1';
$route['gracias/(:num)'] = 'donation/done/$1';