<? defined('BASEPATH') OR exit('No direct script access allowed');
class Project_model extends CI_Model {

	function __construct(){
		parent:: __construct();
		$this->load->database();
	}
	
	function new_project($data){
		$this->db->insert('tb_projects',$data);
		return $this->db->insert_id();
	}
	
	function update_project($id,$data){
		return $this->db->where('id', $id)->update('tb_projects', $data);
	}
	
	function is_my_project($user_id,$project_id){
		$this->db
		->select('p.id')
		->from('tb_projects AS p')
		->where('p.user_id',$user_id)
		->where('p.id',$project_id)
		->limit(1);
		return $this->db->get()->row();
	}
	
	function get_project($project_id){
		$this->db
		->select('p.*, p.id AS project_id, c.slug AS category_slug, u.name, u.email, u.account, u.image AS user_image')
		->from('tb_projects AS p')
		->where('p.id', $project_id)->limit(1)
		->join('tb_users AS u','u.id = p.user_id','inner')
		->join('tb_categories AS c','c.id = p.category','inner');
		$project = $this->db->get()->row();
		$this->db->select('count(*) AS count')->from('tb_backers')
				 ->where('project_id',$project->project_id)->where('paid',1);
		$project->backers = $this->db->get()->row();
		return $project;
	}
	
	function get_categories(){
		$this->db
		->select('c.*')
		->from('tb_categories AS c')
		->where('c.parent',null);
		return $this->db->get()->result();
	}
	
	function get_projects($constrains='',&$total=0){
		if(is_array($constrains)) extract($constrains);
		$this->db
		->select('SQL_CALC_FOUND_ROWS TRUE AS cfr',FALSE)
		->select('p.*, p.id AS project_id, c.slug, u.name')
		->from('tb_projects AS p')
		->order_by('p.id','DESC')
		->join('tb_users AS u','u.id = p.user_id','inner')
		->join('tb_categories AS c','c.id = p.category','inner');
		if(empty($panel)) $this->db->where('p.accepted',1);
		if(isset($category)) if($category) $this->db->where('category',$category);
		if(isset($limit,$offset)) $this->db->limit($limit,$offset);
		if(!empty($main)) $this->db->limit(3);
		if(isset($name)) if($name) $this->db->where("p.title LIKE '%{$this->db->escape_str($name)}%'");
		$result = $this->db->get()->result();
		if(!empty($result)){
			$total = (int)$this->db->select('FOUND_ROWS() AS total')->get()->first_row()->total;
			foreach($result as $k=>$r) $result[$k]->backers = $this->db->select('count(*) AS count')
																		->where('project_id',$r->project_id)
																		->where('paid',1)
																		->get('tb_backers')->row();
		}
		return $result;
	}
	
	function get_projects_by_id($user_id,$constrains='',&$total){
		if(is_array($constrains)) extract($constrains);
		$this->db
		->select('SQL_CALC_FOUND_ROWS TRUE AS cfr',FALSE)
		->select('p.*, p.id AS project_id, c.slug, u.name')
		->from('tb_projects AS p')
		->where('p.user_id',$user_id)
		->order_by('p.id','DESC')
		->join('tb_users AS u','u.id = p.user_id','inner')
		->join('tb_categories AS c','c.id = p.category','inner');
		if(isset($limit,$offset)) $this->db->limit($limit,$offset);
		$result = $this->db->get()->result();
		if(!empty($result)) 
			$total = (int)$this->db->select('FOUND_ROWS() AS total')->get()->first_row()->total;
		foreach($result as $k=>$r) $result[$k]->backers = $this->db->select('count(*) AS count')
																	->where('project_id',$r->project_id)
																	->where('paid',1)
																	->get('tb_backers')->row();
		return $result;
	}
	
	function get_top_project(){
		$this->db->select('p.*, p.id AS project_id, p.image, u.name')->from('tb_projects as p')
		->where('p.accepted = 1')->limit(1)->join('tb_users AS u','u.id = p.user_id','inner');
		$project = $this->db->get()->row();
		if(!empty($project)){
			$this->db->select('count(*) AS count')->from('tb_backers')->where('project_id',$project->project_id)->where('paid',1);
			$project->backers = $this->db->get()->row();
		}
		return $project;
	}
	
	function accept_project($project_id){
		$data = array('start_date' => time(), 'accepted' => 1);
		$this->db->where('id',$project_id)->update('tb_projects',$data);
	}
	
	function reject_project($project_id){
		$this->db->where('id',$project_id)->update('tb_projects', array('accepted' => 2));
	}
	
	function checkout($data){
		$this->db->insert('tb_backers',$data);
		return $this->db->insert_id();
	}
	
	function add_report($data){
		if($this->db->insert('tb_reported_projects',$data))
			return $this->db->insert_id();
		else return false;
	}
	
	function update_amount($project_id){
		$backers = $this->db->select('*')->where('project_id',$project_id)
										 ->where('paid',1)
										 ->get('tb_backers')->result();
		$data['p_amount'] = 0;
		foreach($backers as $backer) $data['p_amount'] += $backer->amount;
		$this->db->where('id',$project_id)->update('tb_projects',$data);
	}
}
?>