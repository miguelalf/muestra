<? defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model {
	
	function __construct(){
		parent:: __construct();
		$this->load->database();
	}
	
	function get_user($user_id){
		$this->db
		->select('u.id AS user_id, u.name, u.image, u.email, u.created_on, u.biography, u.location, u.webs, u.account')
		->from('tb_users AS u')
		->where('u.id',$user_id)
		->limit(1);
		return $this->db->get()->row();
	}
	
	function update_user($user_id,$data){
		return $this->db->where('u.id',$user_id)->update('tb_users AS u', $data);
	}
	
	function activate($email){
		return $this->db->where('email',$email)->update('tb_users',array('activated'=>true));
	}
	
}
?>