$(function(){
	
	$('.textControl .limitedText').keyup(function(e){
		if(e.keyCode == 8 || e.keyCode == 46){
			var number = parseInt($(this).closest('div').find('.remaining-letters').html());
			var limit = parseInt($(this).closest('div').find('.letters-limit').html());
			if(limit > number){
				var value = $(this).closest('div').find('.limitedText').val().length;
				value = limit-value;
				$(this).closest('div').find('.remaining-letters').html(value);
			}
		}
	});
	
	$('.textControl .limitedText').bind('paste',function(e){
		var master = $(this).closest('div');
		setTimeout(function(){
			var value = $(master).find('.limitedText').val().length;
			var limit = $(master).find('.letters-limit').html();
			if(value > limit){
				var theText = $(master).find('.limitedText').val();
				$(master).find('.limitedText').val($.trim(theText).substring(0, limit));
				$(master).find('.remaining-letters').html(0);
			}else{
				value = limit-value;
				$(master).find('.remaining-letters').html(value);
			}
		},10);
	});
	
	$('.textControl .limitedText').keypress(function(e){
		var limit = $(this).closest('div').find('.letters-limit').html();
		var number = $(this).closest('div').find('.remaining-letters').html();
		if(limit-number == limit){
			e.preventDefault();
		}else if($(this).closest('div').find('.limitedText').val().length > limit){
			var theText = $(this).closest('div').find('.limitedText').val();
			$(this).closest('div').find('.limitedText').val($.trim(theText).substring(0, limit));
			$(this).closest('div').find('.remaining-letters').html(0);
		}else{
			var value = $(this).closest('div').find('.limitedText').val().length+1;
			value = limit-value;
			$(this).closest('div').find('.remaining-letters').html(value);
		}
	});
	
});