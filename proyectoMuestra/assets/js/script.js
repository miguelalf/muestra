$(function(){
	
	/***** Vista previa imagen *****/
	$('#imageInput').change(function(){
		if(this.files && this.files[0]){
			var reader = new FileReader();
			reader.onload = function(e){
				selectedImage = e.target.result;
				$('#imagePreview').attr('src', selectedImage);
			};
			reader.readAsDataURL(this.files[0]);
		}
	});
	
	/***** Agregar urls en editar perfil *****/
	$('#add-web').on('click', function(){
		if($('#new-web').val()){
			var value = $('#new-web').val();
			var name = value;
			if(!(value.indexOf('http') != -1)){
				value = 'http://'+value;
			}else{
				var splitString = value.split('//');
				name = splitString[1];
			}
			var webs = $('#webs').val().split(',');
			webs.push(name);
			$('#webs').val(webs.toString());
			var newUrl = '<div class="web-list-item"><a href="'+value+'" target="_blank">'+name+'</a><a href="#" class="remove-link" rel="'+name+'">&times;</a></div>';
			$('#urls-web').append(newUrl);
			$('#new-web').val('').focus();
		}
	});
	
	/***** Quitar url de la lista en editar perfil *****/
	$('#urls-web').on('click', 'a.remove-link', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var index = $(this).attr('rel');
		var webs = $('#webs').val().split(',');
		webs = $.grep(webs, function(e) {
			return e != index;
		});
		$('#webs').val(webs.toString());
		$(this).parent().remove();
	});
	
	/***** Errores *****/
	$('.form-control').bind('input', function() { 
		if($(this).closest('.form-group').hasClass('has-error')){
			$(this).closest('.form-group').removeClass('has-error').find('.required').remove();
		}
	});
	
	/***** Plugins *****/
	var eOptions = {
		text:  '<i class="glyphicon glyphicon-picture"></i> Subir imagen <span>Formato: jpg, png<br>Recomendado: 1024 x 768 px</span>'
	};
	$('#imageInputCustom').ezdz(eOptions);
	if($('#imageName').val()){
		$('#imageInputCustom').ezdz('preview', '../../public/project/'+$('#imageName').val());
	}
	var sOptions = {
		lang: 'es-ES',
		height: 220,
		styleTags: ['h3','p'],
		toolbar: [
			['style', ['bold', 'italic']],
			['pharagrap', ['ul','style']],
			['insert', ['link','picture','video']]
		]
	};
	$('#content').summernote(sOptions);
	$('.dropdown-menu.dropdown-style').find('h3').text('Titulo');
	$('.dropdown-menu.dropdown-style').find('p').text('Parrafo');
	
	/***** Affix *****/
	$('.box-affix').affix({
		offset: {
			top: 0,
			bottom: function () {
				return (this.bottom = $('#footer').outerHeight(true) + 80) 
			}
		}
	});
	
});