var errorAlert = '<p class="alert alert-warning text-center" id="alert-ajax-error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">cerrar</span></button>No se pudo completar tu solicitud. <span class="dis-iblock">Por favor intenta de nuevo.<span></p>';

var modalErrorAlert = errorAlert.replace('data-dismiss="alert"', 'data-dismiss="modal"');

function load_ajax(url, $target, flag_replace, method, data){
		
	method = typeof method !== 'undefined' ? method : 'get';
    data   = typeof data !== 'undefined' ? data : {};
		
	$target.addClass('loading').find('#alert-ajax-error').remove();
	var $request;
	
	if(method === 'post'){
		$request = $.post(url, data);
	}else if(method === 'get'){
		$request = $.get(url, data);
	}
	
	$request
		.success(function(d){
			if(flag_replace === true){
				$target.replaceWith(d);
			}
			else{
				$target.html(d);
			}
		})
		.error(function(m){
			if(flag_replace === true){
				$target.replaceWith(errorAlert);
			}
			else{
				$target.prepend(errorAlert);
			}
		})
		.complete(function(){
			$target.removeClass('loading');
		});
}

var $modal, $modalContent;
function open_in_modal(url){
	$modalContent.addClass('loading');
	$modal.modal('show');
	
	$.get(url)
		.success(function(d){
			$modalContent.replaceWith(d);
			$modalContent = $('.modal-dialog', $modal);
		})
		.error(function(m){
			$modalContent.html(modalErrorAlert).removeClass('loading');
		});
}

$(function(){

	$modal = $('#modal').modal({backdrop: true, show: false});
	$modalContent = $('.modal-dialog', $modal);
		
	$modal.on('hidden.bs.modal', function(){
		$(this).find('.modal-dialog').empty();
	});
	
	//cargar href en modal
	$('body').on('click','[rel~=modal-ajax]',function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
				
		var url = !(url = $(this).attr('href')) ? $(this).attr('data-url') : url;
		
		$modalContent.addClass('loading');
		$modal.one('shown.bs.modal', function(){
			$.get(url)
				.success(function(d){
					$modalContent.replaceWith(d);
					$modalContent = $('.modal-dialog', $modal);
				})
				.error(function(m){
					$modalContent.html(modalErrorAlert).removeClass('loading');
				});
		});
		
		if($modal.hasClass('in')){
			$modal.trigger('shown.bs.modal');
		}else{
			$modal.modal('show');
		}
		
		
	});
	
	$('body').on('submit', 'form.modal-ajax', function(e){
		e.preventDefault();
		var $form = $(this),
			url   	= (url = $form.attr('data-url')) ? url : $form.attr('action');
			
		$modalContent.addClass('loading');
		$modal.modal('show');
			
		$form.ajaxSubmit({
			url: url,
			success: function(d){
				$modalContent.replaceWith(d);
				$modalContent = $('.modal-dialog', $modal);
			},
			error: function(m){
				$modalContent.html(modalErrorAlert).removeClass('loading');
			}
		});
	});
	
	//cargar formas de modal en la misma modal
	$modal.on('submit','form',function(e){
		e.preventDefault();
		var $form = $(this),
			url   = $form.attr('action');
					
		$modalContent.addClass('loading');
		
		$form.ajaxSubmit({
			url: url,
			success: function(d){
				$modalContent.replaceWith(d);
				$modalContent = $('.modal-dialog', $modal);
			},
			error: function(m){
				$modalContent.html(modalErrorAlert).removeClass('loading');
			}
		});
	});
	
	//cargar contenido ajax en un elemento
	$('body').on('click','[rel~=inline-ajax]',function(e){
		e.preventDefault();
		var $this = $(this),
			url   	= (url = $this.attr('href')) ? url : $this.attr('data-url'),
			target  = $this.attr('data-target'),
			$target = $(target),
			flag_replace = $this.attr('data-replace') ? true : false;
		
		$this.blur();
		if($target.length){
			load_ajax(url, $target, flag_replace);
		}
	});
	
	$('body').on('submit', 'form.inline-ajax', function(e){
		e.preventDefault();
		var $this = $(this),
			url   	= (url = $this.attr('data-url')) ? url : $this.attr('action'),
			target  = $this.attr('data-target'),
			$target = $(target),
			flag_replace = $this.attr('data-replace') ? true : false;
			
		if($target.length){
			load_ajax(url, $target, flag_replace, $this.attr('method'), $this.serialize());
		}
	});
	
	//cargar forma para editar via ajax
	$('body').on('click','[rel~=edit-ajax]',function(e){
		e.preventDefault();
		var $this = $(this),
			url   	= (url = $this.attr('href')) ? url : $this.attr('data-url'),
			target  = $this.attr('data-target'),
			$target = $(target);
		
		$this.blur();
		if($target.length){
			$target.addClass('loading').off('submit.editable')
			$('.alert', $target).remove();
			
			var $content = $target.html();
			
			$.get(url)
				.success(function(d){
					$target.html(d);
				})
				.error(function(m){
					$target.prepend(errorAlert);
					//$target.addClass('editable');
				})
				.complete(function(){
					$target.removeClass('loading');
				});
			
			$target.one('reset', 'form', function(){
				$target.html($content);
				//$target.addClass('editable');
			});
			
			$target.on('submit.editable', 'form', function(e){
				e.preventDefault();
								
				$target.addClass('loading');
				$('.alert', $target).remove();
				
				var $form = $(this),
					url   = $form.attr('action'),
					data  = $form.serialize();
				
				$modalContent.addClass('loading');
								
				$.post(url,data)
					.success(function(d){
						$target.html(d);
						//$target.addClass('editable');
					})
					.error(function(m){
						$target.prepend(errorAlert);
					})
					.complete(function(){
						$target.removeClass('loading');
					});
			});
		}
	});
});