$(function(){
	OpenPay.setId('');
	OpenPay.setApiKey('');
	OpenPay.setSandboxMode(true);
	var deviceSessionId = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");
	
	$('#pay-button').on('click', function(event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		var flag = false;
		$("#pay-button").prop( "disabled", true);
		if($('input[name="firstName"]').val().length == 0){
			$('input[name="firstName"]').closest('.form-group').addClass('has-error').find('.required').text('Campo obligatorio');
			flag = true;
		}
		if($('input[name="email"]').val().length == 0){
			$('input[name="email"]').closest('.form-group').addClass('has-error').find('.required').text('Campo obligatorio');
			flag = true;
		}
		if(!flag)
			OpenPay.token.extractFormAndCreate('payment-form', sucess_callbak, error_callbak);  
		else
			$("#pay-button").prop("disabled", false);
	});

	var sucess_callbak = function(response) {
		var token_id = response.data.id;
		$('#token_id').val(token_id);
		$('#payment-form').submit();
	};

	var error_callbak = function(response) {
		var desc = response.data.description != undefined ? response.data.description : response.message;
		console.log(JSON.stringify(response));
		$('#alerts').html('');
		switch(response.data.error_code){
			case 2005:
				$('#alerts').append('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Tarjeta ya expirada</div>');
				break;
			case 2006:
				$('#alerts').append('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Número CVN es obligatorio</div>');
				break;
			case 1001:
				$('#alerts').append('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Nombre del titular y número de tarjeta son obligatorios</div>');
				break;
		}
		$('html, body').animate({ scrollTop: $('#alerts').offset().top-100 }, 300, 'linear');
		$("#pay-button").prop("disabled", false);
	};
});