<?php
	/*
	 * PROCESO PARA OBTENER Y REGISTRAR LA INFORMACION DE ADUASIS AEREO, TERRESTRE, MARITIMO, NUEVO LAREDO Y TFS.
	 * GENERADO POR: MIGUEL VALDES
	 * FECHA INICIO: 30 NOVIEMBRE 2017 17:45
	 */

	namespace App\Crones;
	
	$rutaSufijo = "/.../";
	
	require_once __DIR__ . '/../vendor/autoload.php';

	use App\Core\Database;
	use App\Modelos\Trafico;
	use App\Modelos\TraficoGuia;
	use App\Modelos\TraficoSello;
	use App\Modelos\TraficoFactura;
	use App\Modelos\TraficoPedimento;
	use App\Modelos\TraficoClienteAM;
	use App\Modelos\TraficoContenedor;
	use App\Modelos\TraficoFacturaCove;
	use App\Modelos\TraficoPedimentoCaso;
	use App\Modelos\TraficoFacturaEmbarque;
	use App\Modelos\Dependencia\ControlLog;
	use App\Modelos\TraficoFacturaVehiculo;
	use App\Modelos\TraficoPedimentoTransportista;
	use App\Modelos\Dependencia\OperacionReferencia;
	
	use Illuminate\Database\Capsule\Manager as DB;

	// Inicializar el conector base de datos
	$db_driver = (new Database)->init();

	// Lista de procesos
	$procesos = [
		'Todos', // 0
		'Pedimento', // 1
		'Identificadores', // 2
		'Transportista', // 3
		'Contenedores', // 4
		'Sellos', // 5
		'Guias', // 6
		'Embarque', // 7
		'Vehiculo', // 8
		'Facturas', // 9
		'Coves', // 10 - Dentro de facturas
	];
	
	// Especificar la lista de los tipos de aduana
	$tipos_aduanas = [
		'A' => ['...'],
		'M' => ['...'],
		'T' => ['...','...','...'],
	];
	
	// Configuracion
	// Obtener valores por parametro
	$proceso = 0;
	$restricciones = [];
	$conexion = $rutaSufijo == "/.../" ? "..." : "default";
	
	if(!empty($_GET['dias']) and $_GET['dias'] > 0){ // Dias atras 
		$restricciones['dias'] = $_GET['dias'];
	}
	
	if(!empty($_GET['aduanas']) and strlen($_GET['aduanas']) > 0){ // Tipo de aduana
		$restricciones['aduanas'] = explode(',', $_GET['aduanas']);
		
		// Buscar tipos aduanas que no se van a usar
		$aduanas_buscar = array_flip($restricciones['aduanas']);
		$aduanas_ignorar = array_diff_key($tipos_aduanas, $aduanas_buscar);
		foreach($aduanas_ignorar as $id => $elemento){
			unset($tipos_aduanas[$id]);
		}
	}
	
	if(!empty($_GET['estatus']) and strlen($_GET['estatus']) > 0){ // Estatus
		$restricciones['estatus'] = explode(',', $_GET['estatus']); 
	}
	
	if(!empty($_GET['referencia']) and strlen($_GET['referencia']) > 4){ // Referencia
		$restricciones['referencia'] = $_GET['referencia'];
	}
	
	if(!empty($_GET['proceso']) and $_GET['proceso'] > 0){ // Proceso
		$proceso = $_GET['proceso'];
	}
	
	if(!empty($_GET['am']) && $_GET['am'] == 1){
		$aduanas = [
			'A' => ['ged_aereo'] 
		];
		
		foreach($aduanas as $tipo_aduana => $lista_aduanas){
			foreach($lista_aduanas as $aduana_am){
				if($proceso == 101){
					try{
						$clientes = DB::connection($aduana_am)->table('FCLIENTE')
							->select('ID_CLIENTE AS id','RAZON_SOCIAL AS razon_social', 'RFC AS rfc')
							->whereNotNull('RAZON_SOCIAL')
							->where(DB::raw('LEN(RFC)'),'>','3')
							->orderBy('RAZON_SOCIAL')
							->get()
							->all();
							
						if(!empty($clientes)){
							foreach($clientes as $_cliente){
								try{
									$cliente = DB::connection($conexion)->table('...')
										->where('razon_social', trim($_cliente->razon_social))
										->where('rfc', trim($_cliente->rfc))
										->where('estatus','<>','C')
										->first();
										
									if(!empty($cliente)) continue;
									
									$cliente = new TraficoClienteAM;
									$cliente->id = $_cliente->id;
									$cliente->razon_social = trim($_cliente->razon_social);
									$cliente->rfc = trim($_cliente->rfc);
									$cliente->origen = $aduana_am;
									$cliente->fecha_registro = date('Y-m-d H:i:s', time());
									
									try{
										if($cliente->save()){
											echo sprintf("SE GUARDO EL CLIENTE %s CON LA CLAVE %s", $cliente->razon_social, $cliente->clave);
										}
									}
									catch(\PDOException $ex){ echo sprintf("ERROR AL GUARDAR CLIENTE %s - %s \n\r", $cliente->razon_social, $ex->getMessage()); }
									catch(\Exception $ex){ echo sprintf("ERROR AL GUARDAR CLIENTE %s - %s \n\r", $cliente->razon_social, $ex->getMessage()); }
								}
								catch(\PDOException $ex){ }
								catch(\Exception $ex){ }
							}
						}
					}
					catch(\PDOException $ex){ echo sprintf("ERROR AL OBTENER LOS CLIENTES - %s \n\r", $ex->getMessage()); }
					catch(\Exception $ex){ echo sprintf("ERROR AL OBTENER LOS CLIENTES - %s \n\r", $ex->getMessage()); }
				}
			}
		}
		
		die('---  PROCESO AM TEMINADO  ---');
	}
	
	// Inicio del proceso
	// Recorrer en todos los tipos de aduanas
	foreach($tipos_aduanas as $tipo => $tipo_aduana){
		foreach($tipo_aduana as $origen){
			if(!empty($referencias = Trafico::traerReferencias($restricciones))){
				
				foreach($referencias as $operacion_aduasis){
					if($operacion_aduasis->tipo == $tipo){
						
						// Crear objeto operacion
						$operacion = new OperacionReferencia($origen,$operacion_aduasis);
						
						// Proceso 1 - Pedimento
						if($proceso == 0 or $proceso == 1){
							try{
								$operacion->proceso = 1;
								$pedimentos = TraficoPedimento::query($operacion)->get()->all();
								
								if(!empty($pedimentos)){
									ControlLog::mostrar($operacion, $pedimentos, $procesos);
									
									// Procesar pedimentos
									foreach($pedimentos as $pedimento){
										$pedimento = get_object_vars($pedimento);
										try{
											$pedimento = TraficoPedimento::updateOrCreate(['referencia' => $operacion->clave], $pedimento);
										}
										catch(PDOException $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
										catch(Exception $ex){
											ControlLog::mostrarEx($operacion, $ex, 1);
										}
									}
								}else{
									continue; // En caso de no encontrar pedimento pasar a la siguiente referencia
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 1
						
						// Proceso 2 - Pedimento Casos (Identificadores)
						if($proceso == 0 or $proceso == 2){
							try{
								$operacion->proceso = 2;
								$pedimento_casos = TraficoPedimentoCaso::query($operacion)->get()->all();
								
								if(!empty($pedimento_casos)){
									ControlLog::mostrar($operacion, $pedimento_casos, $procesos);
									
									// Procesar identificadores
									foreach($pedimento_casos as $caso){
										$caso = get_object_vars($caso);
										
										$unico = [
											'referencia' => $operacion->clave,
											'secuencial' => $caso['secuencial']
										];
										
										try{
											$caso = TraficoPedimentoCaso::updateOrCreate($unico, $caso);
											
											if(!empty($caso)){
												$operacion->pedimento_casos_activos[] = $caso->clave;
											}
										}
										catch(PDOException $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
										catch(Exception $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
									}
									
									TraficoPedimentoCaso::removerInactivos($operacion, 'pedimento_casos_activos');
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 2
						
						// Proceso 3 - Pedimento Transportista
						if($proceso == 0 or $proceso == 3){
							try{
								$operacion->proceso = 3;
								$transportistas = TraficoPedimentoTransportista::query($operacion)->get()->all();
								
								if(!empty($transportistas)){
									ControlLog::mostrar($operacion, $transportistas, $procesos);
									
									foreach($transportistas as $transportista){
										$transportista = get_object_vars($transportista);
										
										$unico = [
											'referencia' => $operacion->clave,
											'id_linea_fletera' => $transportista['id_linea_fletera'],
										];
										
										try{
											$transportista = TraficoPedimentoTransportista::updateOrCreate($unico, $transportista);
											
											if(!empty($transportista)){
												$operacion->transportistas_activos[] = $transportista->clave;
											}
										}
										catch(PDOException $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
										catch(Exception $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
									}
									
									TraficoPedimentoTransportista::removerInactivos($operacion, 'transportistas_activos');
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 3
						
						// Proceso 4 - Contenedores
						if($proceso == 0 or $proceso == 4){
							try{
								$operacion->proceso = 4;
								$contenedores = TraficoContenedor::query($operacion)->get()->all();
								
								if(!empty($contenedores)){
									ControlLog::mostrar($operacion, $contenedores, $procesos);
									
									foreach($contenedores as $contenedor){
										$info_extra = TraficoContenedor::extra($operacion)->first();
										
										$contenedor->estatus = 'A';
										$contenedor->cliente = $info_extra->cliente;
										$contenedor->aduana = $info_extra->aduana;
										$contenedor->fecha = $info_extra->fecha;
										$contenedor->gempresarial = $info_extra->gempresarial;
										$contenedor->peso = (float)$contenedor->peso;
										$contenedor->contenedor = trim($contenedor->contenedor);
										if(empty($contenedor->material_peligroso)) $contenedor->material_peligroso = 0;
										
										$contenedor = get_object_vars($contenedor);
										
										$unico = [
											'referencia' => $operacion->clave,
											'contenedor' => $contenedor['contenedor']
										];
										
										try{
											$contenedor = TraficoContenedor::updateOrCreate($unico, $contenedor);
											
											if(!empty($contenedor)){
												$operacion->contenedores_activos[] = $contenedor->clave;
											}
										}
										catch(PDOException $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
										catch(Exception $ex){
											ControlLog::mostrarEx($operacion, $ex, 1);
										}
									}
									
									TraficoContenedor::removerInactivos($operacion, 'contenedores_activos');
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 4
						
						// Proceso 5 - Sellos
						if($proceso == 0 or $proceso == 5){
							try{
								$operacion->proceso = 5;
								$sellos = TraficoSello::query($operacion)->get()->all();
								
								if(!empty($sellos)){
									ControlLog::mostrar($operacion, $sellos, $procesos);
									
									foreach($sellos as $sello){
										$sello = get_object_vars($sello);
								
										$sello['sello'] = trim($sello['sello']);
										$sello['numero_vehiculo'] = trim($sello['numero_vehiculo']);
										$sello['consecutivo'] = trim($sello['consecutivo']);
										
										$unico = [
											'referencia' => $operacion->clave,
											'numero_vehiculo' => $sello['numero_vehiculo'],
											'sello' => $sello['sello'],
											'consecutivo' => $sello['consecutivo']
										];
										
										try{
											$sello = TraficoSello::updateOrCreate($unico, $sello);
											
											if(!empty($sello)){
												$operacion->sellos_activos[] = $sello->clave;
											}
										}
										catch(PDOException $ex){
											ControlLog::mostrarPDOEx($operacion, $ex, 1);
										}
										catch(Exception $ex){
											ControlLog::mostrarEx($operacion, $ex, 1);
										}
									}
									
									TraficoSello::removerInactivos($operacion, 'sellos_activos');
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 5
						
						// Proceso 6 - Guias
						if($proceso == 0 or $proceso == 6){
							try{
								$operacion->proceso = 6;
								$guias = TraficoGuia::query($operacion)->get()->all();
								
								if(!empty($guias)){
									ControlLog::mostrar($operacion, $guias, $procesos);
									
									foreach($guias as $guia){
										$guia->referencia = $operacion->clave;
										$guia->tipo = $guia->cve_tipo_guia;
										
										$guia = get_object_vars($guia);
										
										$operacion->guias_activas[] = $guia['guia'];
										
										$guia_radar = TraficoGuia::enRadar($operacion, $guia['guia'])->first();
											
										if(empty($guia_radar)){
											$guia_radar = TraficoGuia::create($guia);
										}
									}
									
									TraficoGuia::remover($operacion);
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 6
						
						// Proceso 7 - Embarque
						if($proceso == 0 or $proceso == 7){
							try{
								$operacion->proceso = 7;
								$embarque = TraficoFacturaEmbarque::query($operacion)->first();
								
								if(!empty($embarque)){
									ControlLog::mostrar($operacion, [$embarque], $procesos);
									
									$embarque = get_object_vars($embarque);
									
									try{
										$embarque = TraficoFacturaEmbarque::updateOrCreate(['referencia' => $operacion->clave], $embarque);
									}
									catch(PDOException $ex){
										ControlLog::mostrarPDOEx($operacion, $ex, 1);
									}
									catch(Exception $ex){
										ControlLog::mostrarEx($operacion, $ex, 1);
									}
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 7
						
						// Proceso 8 - Vehiculo (Embarques)
						if($proceso == 0 or $proceso == 8){
							try{
								$operacion->proceso = 8;
								$vehiculo = TraficoFacturaVehiculo::query($operacion)->first();
								
								if(!empty($vehiculo)){
									ControlLog::mostrar($operacion, [$vehiculo], $procesos);
									
									$vehiculo = get_object_vars($vehiculo);
									
									try{
										$vehiculo = TraficoFacturaVehiculo::updateOrCreate(['referencia' => $operacion->clave], $vehiculo);
									}
									catch(PDOException $ex){
										ControlLog::mostrarPDOEx($operacion, $ex, 1);
									}
									catch(Exception $ex){
										ControlLog::mostrarEx($operacion, $ex, 1);
									}
								}
							}
							catch(PDOException $ex){
								ControlLog::mostrarPDOEx($operacion, $ex, 0);
							}
							catch(Exception $ex){
								ControlLog::mostrarEx($operacion, $ex, 0);
							}
						}
						// Fin Proceso 8
					}
				}
			}
		}
	}
	
	die('-- Proceso Terminado --');