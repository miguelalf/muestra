<?php

namespace App\Modelos;

use App\Modelos\MyModel;
use Illuminate\Database\Capsule\Manager as DB;
use App\Modelos\Dependencia\OperacionReferencia;

class TraficoPedimento extends MyModel{
	
	protected $primaryKey = 'referencia';
	protected $table = '';
	
	protected $fillable = [/*...*/];
	
	public $incrementing = false;
	public $timestamps = false;
	
	static public function query(OperacionReferencia $operacion){
		return DB::connection($operacion->origen)->table('fpedimento AS p')
			->select('p.id_tipo_pedimento AS tipo_pedimento','p.id_patente AS patente','p.id_patente_transito AS patente_transito',
				'p.id_regimen AS regimen','p.id_cliente AS cliente','p.razon_social_cliente AS cliente_razon_social',
				'p.rfc_cliente AS cliente_rfc','p.calle_cliente AS cliente_calle','p.numero_cliente AS cliente_numero',
				'p.cp_cliente AS cliente_cp','p.id_municipio AS municipio','p.id_almacen_fiscal AS almacen_fiscal',
				'p.descripcion_alm_fiscal AS desc_almacen_fiscal','p.id_pais_origen AS pais_origen','p.id_pais_vendedor AS pais_vendedor',
				'p.id_apoderado AS apoderado','p.apoderado AS apoderado_nombre','p.rfc_apoderado AS apoderado_rfc',
				'p.curp_apoderado AS apoderado_curp','p.*')
			->selectRaw("(p.id_aduana + p.seccion) AS aduana")
			->selectRaw("(p.id_aduana_transito + p.seccion_transito) AS aduana_transito")
			->selectRaw("(SELECT descripcion FROM fmedio_transporte AS mt WHERE p.medio_trans_salida = mt.cve_transporte) AS medio_trans_salida")
			->selectRaw("(SELECT descripcion FROM fmedio_transporte AS fmt WHERE p.medio_trans_arribo = fmt.cve_transporte) AS medio_trans_arribo")
			->where('p.id_referencia',$operacion->referencia)
			->where('p.id_rectificacion',$operacion->rectificacion);
	}

}