<?php

namespace App\Modelos;

use App\Core\Database;
use Illuminate\Database\Eloquent\Model;
use App\Modelos\Dependencia\OperacionReferencia;

abstract class MyModel extends Model {
	protected static $driver;
	
	/**
	 * Retorna un array que contiene la lista completa del modelo ordenado por su clave
	 * Formato de retorno: ['clave_objeto' => 'objeto_original'].
	 * @param boolean $status Retornar solo los registros con estatus activo (default: false)
	 * @return array() (collection)
	 */
	public static function lista($status = false) {
		if ($status) {
			$object = self::where('estatus','A')->get(); 
		}
		else {
			$object = self::all();
		}
		
		return $object->mapWithKeys(function($object, $key) {
				return [$object->clave => $object->original];
			})->all();
	}
	
	/**
	 * Crea registro en el origen seleccionado.
	 * @param array $datos Mapa de datos a insertar.
	 * @return this
	 */
	public function crear($datos){
		foreach($datos as $cve => $dato){ $this->{$cve} = $dato; }
		return $this->save();
	}
	
	/**
	 * Crear o actualiza un modelo con una conexion configurable.
	 * @param array $unico Arreglo del valor unico en la DB.
	 * @param array $info Arreglo con los campos a insertar.
	 * @param string $conexion Nombre de la conexion a usar.
	 * @return objeto instancia del modelo usado.
	 */
	static public function crearConOtraConexion($unico,$info,$conexion = 'default'){
		$self = new static;
		$self->connection = $conexion;
		return self::updateOrCreate($unico,$info);
	}
	
	/**
	 * Devuelve la referencia partida
	 * @param string $trafico->referencia
	 * @return [referencia, rectificacion]
	 */
	static public function partirReferencia($referencia) {
		if (strpos($referencia, '_') === false) {
			$ref = strtoupper($referencia);
			$rec = 0;
		}
		else {
			$datos = explode("_", strtoupper($referencia));
			$ref = $datos[0];
			$rec = $datos[1];
			if(is_string($rec) and strpos($rec,'R')){
				$rec = substr($rec, -1, 1);
			}
		}
		return ['referencia' => $ref, 'rectificacion' => $rec];
	}
	
	/**
	 * Remueve los registros eliminados de aduasis - Busqueda por clave en el modelo.
	 * @param OperacionReferencia $operacion Toda la informacion de la operacion de trafico.
	 * @param string $etiqueta Nombre de la propiedad que contiene los registros activos.
	 * @returnInt Elementos removidos.
	 */
	static public function removerInactivos(OperacionReferencia $operacion, $etiqueta){
		return self::where('referencia',$operacion->clave)
			->whereNotIn('clave',$operacion->{$etiqueta})
			->delete();
	}
	
	// Se inicializa el driver de eloquent dentro de la clase para prevenir conflictos.
	public function modoCompatible(){
		self::$driver = (new Database)->init();
		return new static();
	}
}
