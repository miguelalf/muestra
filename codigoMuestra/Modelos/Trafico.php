<?php
namespace App\Modelos;

use \stdClass;
use App\Modelos\MyModel;
use Illuminate\Database\Capsule\Manager as DB;

class Trafico extends MyModel{
	
	protected $primaryKey = 'clave';
	protected $table = '...';
	
	protected $fillable = [/*...*/];
	
	public $timestamps = false;
	public $autoincrement = false;
	
	static public function traerReferencias($restricciones = array(), $origen = 'default'){
		if(is_array($restricciones)) extract($restricciones);
		
		$query = DB::connection($origen)->table('... AS t')
			->select('t.clave','t.referencia','a.tipo')
			->join('aduana AS a','t.aduana_despacho','=','a.clave')
			->where('t.complementaria',0);
			
		if(!empty($estatus) and is_array($estatus)){
			$query->whereIn('t.estatus',$estatus);
		}
		
		if(!empty($aduanas) and is_array($aduanas)){
			$query->whereIn('a.tipo',$aduanas);
		}
		
		if(!empty($referencia) and is_string($referencia)){
			$query->where('t.referencia',$referencia);
		}
		
		if(empty($dias) or $dias <= 0){
			$dias = self::diasRestar();
			$dias = !empty($dias->parametro) ? $dias->parametro : $dias->parametro2;
		}
		
		$query->where('t.impuestos_fechapago','>=',DB::raw("DATE_SUB(NOW(), INTERVAL $dias DAY)"));
	
		return $query->orderBy('t.impuestos_fechapago')->get()->all();
	}
	
	static private function diasRestar($origen = 'default'){
		return DB::connection($origen)->table('parametro')
			->where('clave',350)
			->first();
	}
	
	public function aduana(){
		return $this->hasOne('App\Modelos\Aduana', 'clave', 'aduana_despacho');
	}
	
	public function cliente_info(){
		return $this->hasOne('App\Modelos\Cliente', 'clave', 'cliente');
	}
	
	/* EJECUTA EL STORE PROCEDURE DE ADUASISI PARA CREAR TRAFICO
		CONEXION: aduasis_m (Por defecto)
		CONSUME: sp_genera_bodega
		@params: $lista
	*/
	static public function crearBodega($data = [], $conexion = 'aduasis_m'){
		$resultado = new stdClass;
		$resultado->error = true;
		
		// Lista para comprobar los parametros del SP
		$lista = ['entrada','bodega','usuario','tipo_op','linea_fletera','cve_transporte','peso','cliente',
			'proveedor','aduana','seccion','patente','pedimento','bultos','unidad'];
			
		if(!empty($diferencias = array_diff($lista, array_keys($data)))){
			$resultado->mensaje = 'Faltan los siguientes parametos: ' . implode(', ', $diferencias);
			$resultado->faltantes = $diferencias;
		}else{
			try{
				$respuesta = DB::connection($conexion)->select(DB::connection($conexion)->raw("EXEC sp_genera_bodega 
					@entrada='{$data['entrada']}',
					@bodega='{$data['bodega']}',
					@user='{$data['usuario']}',
					@tipo_oper='{$data['tipo_op']}',
					@linea_fletera='{$data['linea_fletera']}',
					@cve_transporte='{$data['cve_transporte']}',
					@peso='{$data['peso']}',
					@cliente='{$data['cliente']}',
					@proveedor='{$data['proveedor']}',
					@aduana='{$data['aduana']}',
					@seccion='{$data['seccion']}',
					@patente='{$data['patente']}',
					@pedimento='{$data['pedimento']}',
					@cantidad_bultos='{$data['bultos']}',
					@unidad='{$data['unidad']}'"));
				
				if(!empty($respuesta)) {
					if($json = json_decode($respuesta[0]->resultado)){
						$resultado->mensaje = $json->Mensaje;
						$resultado->codigo = $json->resultado;
					}else{
						$resultado->respuesta = $respuesta[0]->resultado;
					}
				} else {
					$resultado->mensaje = 'Sin respuesta del servidor.';
				}
				
				$resultado->error = false;
			}
			catch(\PDOException $ex){
				$resultado->mensaje = $ex->getMessage();
			}
			catch(\Exception $ex){
				$resultado->mensaje = $ex->getMessage();
			}
		}
		
		return $resultado;
	}

}
