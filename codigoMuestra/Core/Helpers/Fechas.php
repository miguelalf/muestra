<?php
	/*
	 * CLASE CON FUNCIONES DE FECHAS
	 * GENERADO POR: MIGUEL VALDES
	 * FECHA INICIO: 10 ABRIL 2018 11:07
	 *
	 */

	namespace App\Core\Helpers;
	
	use App\core\Helpers\Validador;
	
	class Fechas{
		public $formatoSitio = 'd/m/Y';
		public $formatoDB = 'Y-m-d H:i:s';
		
		static function convertir($fecha){
			if(!empty($fecha)){
				if(Validador::fecha($fecha)){
					$obj = new self;
					return date($obj->formatoDB, strtotime(str_replace('/','-',$fecha)));
				}
			}
			return false;
		}
		
		static function imprimir($fecha, $formato = ''){
			$obj = new self;
			if(empty($fecha)) return '';
			return date($formato ? $formato : $obj->formatoSitio, strtotime($fecha));
		}
		
		static function tiempo(){
			$obj = new self;
			return date($obj->formatoDB, time());
		}
		
		static function definir($fecha){
			$obj = new self;
			$fecha = strtotime($fecha) ? : 0;
			return $fecha ? date($obj->formatoSitio, $fecha) : '';
		}
	}